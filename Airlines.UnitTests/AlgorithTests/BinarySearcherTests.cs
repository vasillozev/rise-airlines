﻿using Airlines.Business.AlgorithmClasses;

namespace Airlines.UnitTests.AlgorithTests;

public class BinarySearcherTests
{
    [Fact]
    public void BinarySearchReturnsTrueWhenKeyExists()
    {
        // Arrange
        var sortedList = new SortedList<string, int>
        {
            { "apple", 1 },
            { "banana", 2 },
            { "orange", 3 }
        };

        // Act
        var result = BinarySearcher.BinarySearch(sortedList, "banana");

        // Assert
        Assert.True(result);
    }

    [Fact]
    public void BinarySearchReturnsFalseWhenKeyDoesNotExist()
    {
        // Arrange
        var sortedList = new SortedList<string, int>
        {
            { "apple", 1 },
            { "banana", 2 },
            { "orange", 3 }
        };

        // Act
        var result = BinarySearcher.BinarySearch(sortedList, "grape");

        // Assert
        Assert.False(result);
    }

    [Fact]
    public void BinarySearchReturnsTrueWhenListIsEmpty()
    {
        // Arrange
        var sortedList = new SortedList<string, int>();

        // Act
        var result = BinarySearcher.BinarySearch(sortedList, "anykey");

        // Assert
        Assert.False(result);
    }

    [Fact]
    public void BinarySearchReturnsTrueWhenListContainsSingleElementAndKeyMatches()
    {
        // Arrange
        var sortedList = new SortedList<string, int>
        {
            { "apple", 1 }
        };

        // Act
        var result = BinarySearcher.BinarySearch(sortedList, "apple");

        // Assert
        Assert.True(result);
    }

    [Fact]
    public void BinarySearchReturnsFalseWhenListContainsSingleElementAndKeyDoesNotMatch()
    {
        // Arrange
        var sortedList = new SortedList<string, int>
        {
            { "apple", 1 }
        };

        // Act
        var result = BinarySearcher.BinarySearch(sortedList, "banana");

        // Assert
        Assert.False(result);
    }
}
