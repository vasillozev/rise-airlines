﻿using Airlines.Business.AlgorithmClasses;
using Airlines.Business.Exceptions;

namespace Airlines.UnitTests.AlgorithTests
{
    public class ConsoleValidatorTests
    {
        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData(" ")]
        public void ReadInputEmptyOrNullStringReturnsFalse(string input)
            // Assert
            => Assert.Throws<InvalidInputException>(() => ConsoleValidator.Validate(input));

        [Fact]
        public void ReadInputNonEmptyStringReturnsTrue()
        {
            // Arrange
            var input = "Valid Input";

            // Act
            var result = ConsoleValidator.Validate(input);

            // Assert
            Assert.True(result);
        }
    }
}
