﻿using System;
using System.IO;
public class ConsoleOutput : IDisposable
{
    private readonly StringWriter _stringWriter;
    private readonly TextWriter _originalOutput;

    public ConsoleOutput()
    {
        _stringWriter = new StringWriter();
        _originalOutput = Console.Out;
        Console.SetOut(_stringWriter);
    }

    public string GetOutput() => _stringWriter.ToString().Trim();
    public void Dispose()
    {
        _stringWriter.Dispose();
        Console.SetOut(_originalOutput);
    }
}