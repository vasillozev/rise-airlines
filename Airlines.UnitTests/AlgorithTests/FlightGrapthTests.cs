﻿using Airlines.Business.AlgorithmClasses;
using Airlines.Business.DataClasses;

namespace Airlines.UnitTests.AlgorithTests
{
    public class GraphFLightsTests
    {
        [Fact]
        public void GraphFLightsConstructorInitializesNodesList()
        {
            // Arrange & Act
            var graph = new GraphFLights();

            // Assert
            Assert.NotNull(graph.Nodes);
            Assert.Empty(graph.Nodes);
        }

        [Fact]
        public void GraphFLightsAddAddsNodeToList()
        {
            // Arrange
            var graph = new GraphFLights();
            var node = new Node(new Flight("ABC123", "DepartureAirport", "ArrivalAirport", new CargoAircraft("Boing", 1, 1), 1, 1));

            // Act
            graph.Add(node);

            // Assert
            Assert.Single(graph.Nodes);
            Assert.Contains(node, graph.Nodes);
        }

        [Fact]
        public void GraphFLightsRemoveRemovesNodeFromList()
        {
            // Arrange
            var graph = new GraphFLights();
            var node = new Node(new Flight("ABC123", "DepartureAirport", "ArrivalAirport", new CargoAircraft("Boing", 1, 1), 1, 1));
            graph.Add(node);

            // Act
            graph.Remove(node);

            // Assert
            Assert.Empty(graph.Nodes);
        }

        [Fact]
        public void GraphFLightsGetNodesReturnsAllNodes()
        {
            // Arrange
            var graph = new GraphFLights();
            var node1 = new Node(new Flight("ABC123", "DepartureAirport", "ArrivalAirport", new CargoAircraft("Boing", 1, 1), 1, 1));
            var node2 = new Node(new Flight("XYZ456", "AnotherAirport", "FinalAirport", new CargoAircraft("Boing", 1, 1), 1, 1));
            graph.Add(node1);
            graph.Add(node2);

            // Act
            var nodes = graph.GetNodes();

            // Assert
            Assert.Equal(2, nodes.Count);
            Assert.Contains(node1, nodes);
            Assert.Contains(node2, nodes);
        }

        [Fact]
        public void GraphFLightsGetCountReturnsNumberOfNodes()
        {
            // Arrange
            var graph = new GraphFLights();
            var node1 = new Node(new Flight("ABC123", "DepartureAirport", "ArrivalAirport", new CargoAircraft("Boing", 1, 1), 1, 1));
            var node2 = new Node(new Flight("XYZ456", "AnotherAirport", "FinalAirport", new CargoAircraft("Boing", 1, 1), 1, 1));
            graph.Add(node1);
            graph.Add(node2);

            // Act
            var count = graph.getCount();

            // Assert
            Assert.Equal(2, count);
        }
    }
}