﻿using Airlines.Business.DataClasses;

namespace Airlines.UnitTests.AlgorithTests
{
    public class NodeTests
    {
        [Fact]
        public void NodeConstructorInitializesFlightAndNeighbors()
        {
            // Arrange
            var flight = new Flight("ABC123", "DepartureAirport", "ArrivalAirport", new CargoAircraft("Boing", 1, 1), 1, 1);

            // Act
            var node = new Node(flight);

            // Assert
            Assert.Equal(flight, node.Flight);
            Assert.NotNull(node.getNeighbors());
            Assert.Empty(node.getNeighbors());
        }

        [Fact]
        public void NodeAddNeighbourAddsNeighborWithCost()
        {
            // Arrange
            var flight1 = new Flight("ABC123", "DepartureAirport", "ArrivalAirport", new CargoAircraft("Boing", 1, 1), 1, 1);
            var flight2 = new Flight("XYZ456", "AnotherAirport", "FinalAirport", new CargoAircraft("Boing", 1, 1), 1, 1);
            var node1 = new Node(flight1);
            var node2 = new Node(flight2);
            var cost = 100.0;

            // Act
            node1.AddNeighbour(node2, cost);

            // Assert
            _ = Assert.Single(node1.getNeighbors());
            Assert.Contains(node2, node1.getNeighbors().Keys);
            Assert.Equal(cost, node1.getNeighbors()[node2]);
        }

        [Fact]
        public void NodeGetNameReturnsFlightDepartureAirport()
        {
            // Arrange
            var flight = new Flight("ABC123", "DepartureAirport", "ArrivalAirport", new CargoAircraft("Boing", 1, 1), 1, 1);
            var node = new Node(flight);

            // Act
            var name = node.getName();

            // Assert
            Assert.Equal("ABC123", name);
        }
    }
}