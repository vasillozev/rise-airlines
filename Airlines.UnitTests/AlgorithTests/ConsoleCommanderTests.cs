﻿using Airlines.Business.AlgorithmClasses;
using Airlines.Business.ConsoleCommands;
using Airlines.Business.DataClasses;

namespace Airlines.UnitTests.AlgorithTests
{
    public class AirportManagerTests
    {
        private readonly ConsoleCommander _commander = new();
        [Fact]
        public void DisplayAllAirportsByCityReturnsMatchingAirports()
        {
            // Arrange
            var cityCountryAirports = new Dictionary<string, Airport>
        {
            {"ID1", new Airport("ID1", "New Airport","City1", "Country1")},
            {"ID2", new Airport("ID2","New Airport", "City1", "Country2")},
            {"A3", new Airport("ID3", "New Airport","City2", "Country3")}
        };
            var input = "search, City1, City";

            // Act
            var result = _commander.DisplayAllAirports(input, cityCountryAirports);

            // Assert
            Assert.True(result.Length == 2);
        }

        [Fact]
        public void DisplayAllAirportsByCountryReturnsMatchingAirports()
        {
            // Arrange
            var cityCountryAirports = new Dictionary<string, Airport>
        {
            {"Air1", new Airport("ID1","New Airport", "City1", "Country1")},
            {"Air2", new Airport("ID2","New Airport", "City2", "Country1")},
            {"Air3", new Airport("ID3", "New Airport","City3", "Country2")}
        };
            var input = "search, Country1, Country";

            // Act
            var result = _commander.DisplayAllAirports(input, cityCountryAirports);

            // Assert
            Assert.Equal(new[] { "Air1", "Air2" }, result);
        }

        [Fact]
        public void DisplayAllAirportsInvalidInputDataReturnsEmptyArray()
        {
            // Arrange
            var cityCountryAirports = new Dictionary<string, Airport>
        {
            {"Airport1", new Airport("ID1","New Airport", "City1", "Country1")},
            {"Airport2", new Airport("ID2","New Airport", "City2", "Country2")},
            {"Airport3", new Airport("ID3", "New Airport","City3", "Country3")}
        };
            var input = "search, data, City";

            // Act
            var result = _commander.DisplayAllAirports(input, cityCountryAirports);

            // Assert
            Assert.Empty(result);
        }
        [Fact]
        public void FlightsCommandAddsFlightWhenInputMessageStartsWithRouteAddAndRouteIsEmpty()
        {
            // Arrange
            var flight1 = new Flight("ABC", "JFK", "LAX", new CargoAircraft("Boeing", 0.4, 0.5),1,1);
            var setFlights = new HashSet<Flight> { flight1 };
            var flightRoute = new SingleLinkedList<Flight>();

            // Act
            _ = _commander.FlightsCommand("route add ABC", flightRoute, setFlights);

            // Assert
            _ = Assert.Single(flightRoute);
            Assert.Contains(flight1, flightRoute);
        }

        [Fact]
        public void FlightsCommandAddsFlightWhenInputMessageStartsWithRouteAddAndRouteIsNotEmptyAndCorrectSequence()
        {
            // Arrange
            var flight1 = new Flight("ABC123", "JFK", "LAX", new CargoAircraft("Boeing", 0.4, 0.5), 1, 1);
            var flight2 = new Flight("DEF456", "LAX", "SFO", new CargoAircraft("Boeing", 0.4, 0.5), 1, 1);
            var setFlights = new HashSet<Flight> { flight1, flight2 };
            var flightRoute = new SingleLinkedList<Flight>
        {
            flight1
        };

            // Act
            _ = _commander.FlightsCommand("route add DEF456", flightRoute, setFlights);

            // Assert
            Assert.Equal(2, flightRoute.Count());
            Assert.Contains(flight2, flightRoute);
        }

        [Fact]
        public void FlightsCommandDoesNotAddFlightWhenInputMessageStartsWithRouteAddAndRouteIsNotEmptyAndIncorrectSequence()
        {
            // Arrange
            var flight1 = new Flight("ABC1", "JFK", "LAX", new CargoAircraft("Boeing", 0.4, 0.5), 1, 1);
            var flight2 = new Flight("DEF456", "LAX", "SFO", new CargoAircraft("Boeing", 0.4, 0.5), 1, 1);
            var setFlights = new HashSet<Flight> { flight1, flight2 };
            var flightRoute = new SingleLinkedList<Flight>
        {
            flight1
        };

            // Act
            _ = _commander.FlightsCommand("route add ABC1", flightRoute, setFlights);

            // Assert
            _ = Assert.Single(flightRoute);
        }
        [Fact]
        public void RoutePrintReturnsEmptyListWhenFlightsListIsEmpty()
        {
            // Arrange
            var flights = new SingleLinkedList<Flight>();

            // Act
            var result = _commander.RoutePrint(flights);

            // Assert
            Assert.NotNull(result);
            Assert.Empty(result);
        }

        [Fact]
        public void RoutePrintReturnsCorrectListOfFlightNamesWhenFlightsListIsNotEmpty()
        {
            // Arrange
            var flight1 = new Flight("ABC123", "JFK", "LAX", new CargoAircraft("Boeing", 0.4, 0.5), 1, 1);
            var flight2 = new Flight("DEF456", "LAX", "SFO", new CargoAircraft("Boeing", 0.4, 0.5), 1, 1);
            var flights = new SingleLinkedList<Flight>
        {
            flight1,
            flight2
        };

            // Act
            var result = _commander.RoutePrint(flights);

            // Assert
            Assert.NotNull(result);
            Assert.Equal(2, result.Count);
            Assert.Contains("ABC123", result);
            Assert.Contains("DEF456", result);
        }
        [Fact]
        public void PreciseSearchReturnsTrueWhenSearchTermExistsInAirlinesHashSet()
        {
            // Arrange
            var searchTerm = "search Delta";
            var airlines = new HashSet<string> { "Delta", "United", "American" };

            // Act
            var result = _commander.PreciseSearch(searchTerm, airlines);

            // Assert
            Assert.True(result);
        }

        [Fact]
        public void PreciseSearchReturnsFalseWhenSearchTermDoesNotExistInAirlinesHashSet()
        {
            // Arrange
            var searchTerm = "exist Brit";
            var airlines = new HashSet<string> { "Delta", "United", "American" };

            // Act
            var result = _commander.PreciseSearch(searchTerm, airlines);

            // Assert
            Assert.False(result);
        }
    }
}