﻿using Airlines.Business.AlgorithmClasses;

public class ReaderTests
{
    [Fact]
    public void ReadInputValidFileReturnsExpectedValues()
    {
        // Arrange
        string path = "valid_input_file.csv";
        List<string[]> expectedValues =
        [
            new string[] { "Value1", "Value2", "Value3" },
            new string[] { "Value4", "Value5", "Value6" }
            // Add more expected lines as needed
        ];

        // Create a temporary file with test data
        using (var writer = new StreamWriter(path))
        {
            foreach (var line in expectedValues)
            {
                writer.WriteLine(string.Join(",", line));
            }
        }

        // Act
        var result = Reader.ReadInput(path);

        // Assert
        Assert.NotNull(result);
        Assert.Equal(expectedValues.Count, result.Count);
        for (int i = 0; i < expectedValues.Count; i++)
        {
            Assert.Equal(expectedValues[i], result[i]);
        }

        // Clean up: delete temporary file
        File.Delete(path);
    }

    [Fact]
    public void ReadInputEmptyFileReturnsEmptyList()
    {
        // Arrange
        string path = "empty_file.csv";
        // Create an empty file
        File.Create(path).Close();

        // Act
        var result = Reader.ReadInput(path);

        // Assert
        Assert.NotNull(result);
        Assert.Empty(result);

        // Clean up: delete temporary file
        File.Delete(path);
    }
}
