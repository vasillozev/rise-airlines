﻿using Airlines.Business.AlgorithmClasses;
using Airlines.Business.ConsoleCommands;
using Airlines.Business.DataClasses;
using Airlines.Business.Exceptions;
using Airlines.Business.ReservationBookingSystem;

namespace Airlines.UnitTests.AlgorithTests
{
    public class ReservationBookingSystemCommanderTests
    {
        private readonly ReservationBookingSystemCommander _reservationBookingSystemCommander = new();

        [Fact]
        public void AirportFactoryValidInputReturnsDictionary()
        {
            // Act
            var pathAirports = Path.Combine("Resources", "airports.csv");
            var airportsValues = Reader.ReadInput(pathAirports);
            Dictionary<string, Airport> result = ReservationBookingSystemCommander.AirportListToDictionary(airportsValues);


            // Assert
            Assert.NotNull(result);
            _ = Assert.IsType<Dictionary<string, Airport>>(result);
        }

        [Fact]
        public void AirportFactoryInvalidInputReturnsEmptyDictionary()
        {
            // Act
            var pathAirports = Path.Combine("Resources", "airportsEmpty.csv");
            var airportsValues = Reader.ReadInput(pathAirports);
            Dictionary<string, Airport> result = ReservationBookingSystemCommander.AirportListToDictionary(airportsValues);


            // Assert
            Assert.NotNull(result);
            _ = Assert.IsType<Dictionary<string, Airport>>(result);
            Assert.Empty(result);
        }

        [Fact]
        public void AirlineFactoryReturnsEmptySetWhenPassedEmptyList()
        {
            // Arrange
            var airlinesValues = new List<string[]>();

            // Act
            var result = AirlineFactory(airlinesValues);

            // Assert
            Assert.Empty(result);
        }

        [Fact]
        public void AirlineFactoryReturnsSetWithSingleElementWhenPassedSingleItem()
        {
            // Arrange
            var airlinesValues = new List<string[]>
            {
            new string[] { "Delta" }
            };

            // Act
            var result = AirlineFactory(airlinesValues);

            // Assert
            _ = Assert.Single(result);
            Assert.Contains("Delta", result);
        }

        [Fact]
        public void AirlineFactoryReturnsSetWithUniqueElementsWhenPassedMultipleItems()
        {
            // Arrange
            var airlinesValues = new List<string[]>
            {
            new string[] { "Delta" },

            new string[] { "American Airlines" },

            new string[] { "Delta" },

            new string[] { "United Airlines" }
            };

            // Act
            var result = AirlineFactory(airlinesValues);

            // Assert
            Assert.Equal(3, result.Count);
            Assert.Contains("Delta", result);
            Assert.Contains("American Airlines", result);
            Assert.Contains("United Airlines", result);
        }

        // Method under test
        public HashSet<string> AirlineFactory(IList<string[]> airlinesValues)
        {
            HashSet<string> airlinesSetNames = [];
            foreach (var item in airlinesValues)
            {
                if (airlinesSetNames.Contains(item[0]))
                {
                    continue;
                }
                _ = airlinesSetNames.Add(item[0]);
            }
            return airlinesSetNames;
        }

        [Theory]
        [InlineData(new string[] { "Boeing 737", "200", "5000", "150" }, typeof(PassengerAircraft))]
        public void AircraftFactoryCreatesCorrectAircraftType(string[] aircraftValues, Type expectedType)
            // Assert
            => Assert.Equal("PassengerAircraft", expectedType.Name);

        [Fact]
        public void AirlineFactoryReturnsEmptyHashSetWhenInputFileIsEmpty()
        {
            // Act
            var pathAirlines = Path.Combine("Resources", "airportsEmpty.csv");
            var airlinesValues = Reader.ReadInput(pathAirlines);
            HashSet<string> result = ReservationBookingSystemCommander.AirlineListToHashSet(airlinesValues);

            // Assert
            Assert.NotNull(result);
            Assert.Empty(result);
        }

        [Fact]
        public void AirlineFactoryReturnsCorrectHashSetWhenInputFileHasData()
        {
            // Arrange
            string filePath = Path.Combine("Resources", "airlines.csv");
            var expectedAirlines = new HashSet<string> { "AA", "DL", "UA", "LH", "BA", "AF" };

            // Act
            var pathAirlines = Path.Combine("Resources", "airlines.csv");
            var airlinesValues = Reader.ReadInput(pathAirlines);
            HashSet<string> result = ReservationBookingSystemCommander.AirlineListToHashSet(airlinesValues);

            // Assert
            Assert.NotNull(result);
            Assert.Equal(expectedAirlines, result);
        }

        [Fact]
        public void AirlineFactoryReturnsCorrectHashSetWhenInputFileHasDuplicates()
        {
            // Arrange
            string filePath = Path.Combine("Resources", "airlines.csv");
            var expectedAirlines = new HashSet<string> { "AA", "DL", "UA", "LH", "BA", "AF" };

            // Act
            var pathAirlines = Path.Combine("Resources", "airlines.csv");
            var airlinesValues = Reader.ReadInput(pathAirlines);
            HashSet<string> result = ReservationBookingSystemCommander.AirlineListToHashSet(airlinesValues);

            // Assert
            Assert.NotNull(result);
            Assert.Equal(expectedAirlines, result);
        }


        // Method under test
        [Fact]
        public void FlightFactoryReturnsEmptyHashSetWhenInputPathIsEmpty()
        {
            // Act
            var pathAircrafts = Path.Combine("Resources", "airportsEmpty.csv");
            var aircraftValues = Reader.ReadInput(pathAircrafts);
            HashSet<IAircraft> setAircrafts = ReservationBookingSystemCommander.AircraftListToHashSet(aircraftValues);
            var pathFlights = Path.Combine("Resources", "airportsEmpty.csv");
            var flightsValues = Reader.ReadInput(pathFlights);
            HashSet<Flight> result = ReservationBookingSystemCommander.FlightListToHashSet(flightsValues, setAircrafts);

            // Assert
            Assert.Empty(result);
        }

        [Fact]
        public void FlightFactoryReturnsHashSetWithCorrectCountWhenValidInputsAreProvided()
        {
            // Arrange
            string path = Path.Combine("Resources", "flights.csv");
            HashSet<IAircraft> aircrafts =
        [
            new CargoAircraft("Boeing 747-8F", 140000, 854.5),

            new PassengerAircraft("Airbus A320", 20000, 37.4, 150),

            new CargoAircraft("Gulfstream G650", 0.4, 0.5),

            new CargoAircraft("Gulfstream G650", 0.4, 0.5),

            new CargoAircraft("Gulfstream G650", 0.4, 0.5),

            new CargoAircraft("Gulfstream G650", 0.4, 0.5),
        ];

            // Act
            var pathAircrafts = Path.Combine("Resources", "aircrafts.csv");
            var aircraftValues = Reader.ReadInput(pathAircrafts);
            HashSet<IAircraft> setAircrafts = ReservationBookingSystemCommander.AircraftListToHashSet(aircraftValues);
            var pathFlights = Path.Combine("Resources", "flights.csv");
            var flightsValues = Reader.ReadInput(pathFlights);
            HashSet<Flight> result = ReservationBookingSystemCommander.FlightListToHashSet(flightsValues, setAircrafts);

            // Assert
            Assert.Equal(8, result.Count);
        }

        [Fact]
        public void FlightFactoryReturnsHashSetWithFlightsWithCorrectDataWhenValidInputsAreProvided()
        {
            // Arrange
            string path = Path.Combine("Resources", "flights.csv");
            HashSet<IAircraft> aircrafts =
        [
            new PassengerAircraft("Boeing 747-8F", 0.4, 0.5, 5),

            new PassengerAircraft("Airbus A320", 0.4, 0.5, 5),

            new PassengerAircraft("Gulfstream G650", 0.4, 0.5, 5),
        ];

            // Act
            var pathAircrafts = Path.Combine("Resources", "aircrafts.csv");
            var aircraftValues = Reader.ReadInput(pathAircrafts);
            HashSet<IAircraft> setAircrafts = ReservationBookingSystemCommander.AircraftListToHashSet(aircraftValues);
            var pathFlights = Path.Combine("Resources", "flights.csv");
            var flightsValues = Reader.ReadInput(pathFlights);
            HashSet<Flight> result = ReservationBookingSystemCommander.FlightListToHashSet(flightsValues, setAircrafts);
            // Assert
            Assert.True(result.All(flight => flight.DepartureAirport != null && flight.ArrivalAirport != null && flight.Identifier != null && flight.AircraftModel != null));
        }

        // Test for valid input file
        [Fact]
        public void FlightRoutesFactoryValidPathAndFlightsReturnsFlightNode()
        {
            // Arrange
            var pathAircrafts = Path.Combine("Resources", "aircrafts.csv");
            var aircraftValues = Reader.ReadInput(pathAircrafts);
            HashSet<IAircraft> setAircrafts = ReservationBookingSystemCommander.AircraftListToHashSet(aircraftValues);
            ConsoleCommander consoleCommander = new ConsoleCommander();
            var pathFlights = Path.Combine("Resources", "flights.csv");
            var flightsValues = Reader.ReadInput(pathFlights);
            HashSet<Flight> setFlights = ReservationBookingSystemCommander.FlightListToHashSet(flightsValues, setAircrafts);
            FlightNode? flightRouteSearch = null;

            var flightRoutes = Path.Combine("Resources", "flightRoutes.csv");
            IList<string[]> strings = Reader.ReadInput(flightRoutes);
            flightRouteSearch = ReservationBookingSystemCommander.FlightRoutesFactory(strings, setFlights);

            // Act
            var result = ReservationBookingSystemCommander.FlightRoutesFactory(strings, setFlights);

            // Assert
            Assert.NotNull(result);
        }

        // Test for invalid input file
        [Fact]
        public void FlightRoutesFactoryInvalidFileThrowsRouteFindException()
        {
            // Arrange
            var path = Path.Combine("Resources", "airports.csv");
            var flights = new HashSet<Flight>
            {
                 new Flight("FL156", "DFW", "LAX", new CargoAircraft("Boeing 747-8F", 140000,854.5), 1, 1),

                new Flight("FL124", "ORF", "ORG", new CargoAircraft("Boeing 747-8F", 140000,854.5), 1, 1),

                new Flight("FL455", "ORG", "ORD", new CargoAircraft("Boeing 747-8F", 140000,854.5), 1, 1),

                new Flight("FL789", "ORK", "ATL", new CargoAircraft("Gulfstream G650", 140000,854.5), 1, 1),

                new Flight("FL505", "LAX", "ORK", new CargoAircraft("Boeing 747-8F", 140000,854.5), 1, 1),

                new Flight("FL456", "ATL", "DFW", new CargoAircraft("Boeing 747-8F", 140000,854.5), 1, 1),

            }; // Add necessary flights

            IList<string[]> strings = Reader.ReadInput(path);

            // Act & Assert
            _ = Assert.Throws<InvalidFileExceptionSearchRoute>(() => ReservationBookingSystemCommander.FlightRoutesFactory(strings, flights));
        }

        [Fact]
        public void FlightRouteSearchThrowsExceptionWhenInputLengthIsNotThree()
        {
            // Arrange
            string input = "Invalid input";
            Flight flight = new Flight("FL124", "ORF", "ORG", new CargoAircraft("Boeing 747-8F", 140000, 854.5), 1, 1);
            FlightNode flights = new FlightNode(flight);

            // Act & Assert
            _ = Assert.Throws<RouteFindException>(() => ReservationBookingSystemCommander.FlightRouteFind(input, flights));
        }

        [Fact]
        public void FlightRouteSearchReturnsHashSetWithValidInput()
        {
            // Arrange
            string input = "route find ORF";
            Flight flight = new Flight("FL124", "ORG", "ORF", new CargoAircraft("Boeing 747-8F", 140000, 854.5), 1, 1);
            FlightNode flights = new FlightNode(flight);
            _ = flights.AddChild(new Flight("FL11", "ORF", "ATL", new CargoAircraft("Boeing", 5, 6), 1, 1));
            // Setup flight node with some data

            var pathAircrafts = Path.Combine("Resources", "aircrafts.csv");
            var aircraftValues = Reader.ReadInput(pathAircrafts);
            HashSet<IAircraft> setAircrafts = ReservationBookingSystemCommander.AircraftListToHashSet(aircraftValues);

            var pathFlights = Path.Combine("Resources", "flights.csv");
            var flightsValues = Reader.ReadInput(pathFlights);
            HashSet<Flight> setFlights = ReservationBookingSystemCommander.FlightListToHashSet(flightsValues, setAircrafts);

            // Act
            Queue<FlightNode> result = ReservationBookingSystemCommander.FlightRouteFind(input, flights);

            // Assert
            Assert.NotNull(result);
            // Add more assertions based on the expected behavior of FlightRouteSearch
        }

        [Fact]
        public void FlightRouteSearchReturnsDestinationsWithDirectRoutes()
        {
            // Arrange
            string input = "route find ORG";
            Flight flight1 = new Flight("FL124", "ORF", "ORG", new CargoAircraft("Boeing 747-8F", 140000, 854.5), 1, 1);
            Flight flight2 = new Flight("FL125", "ORG", "AFK", new CargoAircraft("Boeing 747-8F", 140000, 854.5), 1, 1);
            FlightNode flights = new FlightNode(flight1);
            _ = flights.AddChild(flight2);
            // Setup flight node with some data, including direct routes

            // Act
            Queue<FlightNode> result = ReservationBookingSystemCommander.FlightRouteFind(input, flights);
            HashSet<string> expected = [];
            foreach (var item in result)
            {
                expected.Add(item.Value.Identifier);
            }
            // Assert
            Assert.NotNull(result);
            // Assert that the result contains destinations with direct routes
            Assert.Contains("FL124", expected);
        }

        [Fact]
        public void FlightRouteSearchReturnsDestinationsWithIndirectRoutes()
        {
            // Arrange
            string input = "route find LAX";
            var pathAircrafts = Path.Combine("Resources", "aircrafts.csv");
            var aircraftValues = Reader.ReadInput(pathAircrafts);
            HashSet<IAircraft> setAircrafts = ReservationBookingSystemCommander.AircraftListToHashSet(aircraftValues);
            var pathFlights = Path.Combine("Resources", "flights.csv");
            var flightsValues = Reader.ReadInput(pathFlights);
            HashSet<Flight> setFlights = ReservationBookingSystemCommander.FlightListToHashSet(flightsValues, setAircrafts);
            string path = Path.Combine("Resources", "flightRoutes.csv");
            IList<string[]> strings = Reader.ReadInput(path);
            FlightNode flightRoutes = ReservationBookingSystemCommander.FlightRoutesFactory(strings, setFlights);
            // Setup flight node with data containing indirect routes   
            // Act
            Queue<FlightNode> result = ReservationBookingSystemCommander.FlightRouteFind(input, flightRoutes);
            HashSet<string> expected = [];
            foreach (var item in result)
            {
                _ = expected.Add(item.Value.Identifier);
            }
            // Assert
            Assert.NotNull(result);
            Assert.Contains("FL505", expected);
        }

        [Fact]
        public void FlightRouteSearchReturnsDestinationsWithIndirectRoutes2()
        {
            // Arrange
            string input = "route find ORF";
            var pathAircrafts = Path.Combine("Resources", "aircrafts.csv");
            var aircraftValues = Reader.ReadInput(pathAircrafts);
            HashSet<IAircraft> setAircrafts = ReservationBookingSystemCommander.AircraftListToHashSet(aircraftValues);
            var pathFlights = Path.Combine("Resources", "flights.csv");
            var flightsValues = Reader.ReadInput(pathFlights);
            HashSet<Flight> setFlights = ReservationBookingSystemCommander.FlightListToHashSet(flightsValues, setAircrafts);
            string path = Path.Combine("Resources", "flightRoutes.csv");
            IList<string[]> strings = Reader.ReadInput(path);
            FlightNode flightRoutes = ReservationBookingSystemCommander.FlightRoutesFactory(strings, setFlights);
            // Setup flight node with data containing indirect routes   
            // Act
            Queue<FlightNode> result = ReservationBookingSystemCommander.FlightRouteFind(input, flightRoutes);
            HashSet<string> strings1 = [];
            foreach (var item in result)
            {
                strings1.Add(item.Value.Identifier);
            }

            // Assert
            Assert.NotNull(result);

            Assert.Contains("FL505", strings1);

            Assert.Contains("FL789", strings1);

            Assert.Contains("FL456", strings1);
        }

        [Fact]
        public void FlightRouteSearchReturnsExceptionWhenNonExistentDestination()
        {
            // Arrange
            string input = "route find";
            var pathAircrafts = Path.Combine("Resources", "aircrafts.csv");
            var aircraftValues = Reader.ReadInput(pathAircrafts);
            HashSet<IAircraft> setAircrafts = ReservationBookingSystemCommander.AircraftListToHashSet(aircraftValues);
            var pathFlights = Path.Combine("Resources", "flights.csv");
            var flightsValues = Reader.ReadInput(pathFlights);
            HashSet<Flight> setFlights = ReservationBookingSystemCommander.FlightListToHashSet(flightsValues, setAircrafts);
            string path = Path.Combine("Resources", "flightRoutes.csv");
            IList<string[]> strings = Reader.ReadInput(path);
            FlightNode flightRoutes = ReservationBookingSystemCommander.FlightRoutesFactory(strings, setFlights);
            // Setup flight node with data containing indirect routes   
            // Assert
            _ = Assert.Throws<RouteFindException>(() => ReservationBookingSystemCommander.FlightRouteFind(input, flightRoutes));
        }
    }
}
