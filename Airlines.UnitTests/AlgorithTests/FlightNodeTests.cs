﻿using Airlines.Business.AlgorithmClasses;
using Airlines.Business.DataClasses;

namespace Airlines.UnitTests.AlgorithTests;

public class FlightNodeTests
{

    // Test adding a child to FlightNode
    [Fact]
    public void AddChildValidChildReturnsTrue()
    {
        // Arrange
        var flight = new Flight("FL123", "JFK", "LAX", new CargoAircraft("Boeing 747 - 8F", 140000, 854.5), 1, 1);
        var parentNode = new FlightNode(flight);
        var childFlight = new Flight("FL124", "LAX", "JFK", new CargoAircraft("Boeing 747 - 8F", 140000, 854.5), 1, 1);

        // Act
        var result = parentNode.AddChild(childFlight);

        // Assert
        _ = Assert.Single(parentNode.Children);
    }

    // Test traversing FlightNode
    [Fact]
    public void TraverseValidDestinationReturnsQueueOfNodes()
    {
        // Arrange
        var flight1 = new Flight("FL123", "JFK", "LAX", new CargoAircraft("Boeing 747 - 8F", 140000, 854.5), 1, 1);
        var flight2 = new Flight("FL124", "LAX", "ATL", new CargoAircraft("Boeing 747 - 8F", 140000, 854.5), 1, 1);
        var parentNode = new FlightNode(flight1);
        _ = parentNode.AddChild(flight2);
        var destination = "ATL"; // Replace with actual destination

        // Act
        var result = parentNode.Traverse(destination);

        // Assert
        Assert.NotNull(result);
        Assert.NotEmpty(result);
    }

    [Fact]
    public void TraverseValidDestinationInRandomOrderFlightsReturnsQueueOfNodes()
    {
        // Arrange
        var flight1 = new Flight("FL156", "ORF", "ORG", new CargoAircraft("Boeing 747 - 8F", 140000, 854.5), 1, 1);
        var flight3 = new Flight("FL124", "ORG", "ORD", new CargoAircraft("Boeing 747 - 8F", 140000, 854.5), 1, 1);
        var flight5 = new Flight("FL455", "ORK", "ATL", new CargoAircraft("Boeing 747 - 8F", 140000, 854.5), 1, 1);
        var flight4 = new Flight("FL789", "LAX", "ORK", new CargoAircraft("Boeing 747 - 8F", 140000, 854.5), 1, 1);
        var flight2 = new Flight("FL505", "ATL", "ORF", new CargoAircraft("Boeing 747 - 8F", 140000, 854.5), 1, 1);
        var flight6 = new Flight("FL456", "DFW", "LAX", new CargoAircraft("Boeing 747 - 8F", 140000, 854.5), 1, 1);

        var parentNode = new FlightNode(flight6);
        _ = parentNode.AddChild(flight4);
        _ = parentNode.AddChild(flight5);
        _ = parentNode.AddChild(flight2);
        _ = parentNode.AddChild(flight1);
        _ = parentNode.AddChild(flight3);
        var destination = "ATL";

        // Act
        var result = parentNode.Traverse(destination);

        // Assert
        Assert.NotNull(result);
        Assert.NotEmpty(result);
    }

    [Fact]
    public void FlattenReturnsCorrectSequence()
    {
        // Arrange
        var flightNode = new FlightNode(new Flight("A", "ABC", "XYZ", new CargoAircraft("A", 6, 7), 1, 1));
        flightNode.AddChild(new Flight("A", "XYZ", "DEF", new CargoAircraft("A", 6, 7), 1, 1));
        flightNode.AddChild(new Flight("A", "XYZ", "GHI", new CargoAircraft("A", 6, 7), 1, 1));

        // Act
        var result = flightNode.Flatten();

        // Assert
        Assert.Equal(new[] { "A", "A", "A" }, result);
    }
}