﻿using Airlines.Business.AlgorithmClasses;
using Airlines.Business.DataClasses;
using Airlines.Business.Exceptions;
using Airlines.Business.ReservationBookingSystem;

namespace Airlines.UnitTests.AlgorithTests;
public class GraphUnitTests
{

    [Fact]
    public void GraphConstructorPopulatesAirportsFlightsCorrectly()
    {
        // Arrange
        var flights = new HashSet<Flight>
            {
                new Flight("ABC", "ABC", "123", new CargoAircraft("A", 6, 7), 1, 1),
                new Flight("XYZ", "XYZ", "456", new CargoAircraft("A", 6, 7), 1, 1),
                new Flight("XYZ", "XYZ", "789", new CargoAircraft("A", 6, 7), 1, 1)
            };

        // Act
        var graph = new Graph(flights);

        // Assert
        Assert.Equal(2, graph.AirportsFlights.Count);
        Assert.True(graph.AirportsFlights.ContainsKey("ABC"));
        Assert.True(graph.AirportsFlights.ContainsKey("XYZ"));
        Assert.Equal(1, graph.AirportsFlights["ABC"].Count);
        Assert.Equal(2, graph.AirportsFlights["XYZ"].Count);
    }

    [Fact]
    public void RouteCheckReturnsTrueWhenRouteExists()
    {
        // Arrange
        HashSet<Flight> flights = [];
        FlightNode flightNode = new FlightNode(new Flight("ABC", "ABC", "DEF", new CargoAircraft("A", 6, 7), 1, 1));
        flights.Add(flightNode.Value);
        var graph = new Graph(flights);
        graph.Children.Enqueue(flightNode);
        // Act
        // Assert
        Assert.Single(graph.Children);
    }

    [Fact]
    public void RouteCheckReturnsFalseWhenRouteDoesNotExist()
    {
        
        HashSet<Flight> flights = [];
        FlightNode flightNode = new FlightNode(new Flight("ABC", "ABC", "DEF", new CargoAircraft("A", 6, 7), 1, 1));
        FlightNode flightNode1 = new FlightNode(new Flight("GHI", "GHI", "DEF", new CargoAircraft("A", 6, 7), 1, 1));
        flights.Add(flightNode.Value);
        flights.Add(flightNode1.Value);
        var graph = new Graph(flights);
        graph.Children.Enqueue(flightNode);
        graph.Children.Enqueue(flightNode1);
        // Assert
        Assert.Throws<RouteFindException>(() => graph.RouteCheck("GHI", "ABC"));
    }

    [Fact]
    public void RouteSearchReturnsCorrectRouteWhenRouteExists()
    {
        // Arrange
        var flights = new HashSet<Flight>
            {
               new Flight("ABC", "ABC", "XYZ", new CargoAircraft("A", 6, 7), 1, 1),
                new Flight("XYZ", "XYZ", "XYT", new CargoAircraft("A", 6, 7), 1, 1),
                new Flight("XYZ", "XYT", "ABC", new CargoAircraft("A", 6, 7), 1, 1),
                new Flight("DEF", "JKL", "MNO", new CargoAircraft("A", 6, 7), 1, 1),
                new Flight("GHI", "MNO", "202", new CargoAircraft("A", 6, 7), 1, 1)
            };
        FlightNode flightNode = new FlightNode(new Flight("ABC", "ABC", "XYZ", new CargoAircraft("A", 6, 7), 1, 1));
        FlightNode flightNode1 = new FlightNode(new Flight("XYZ", "XYZ", "XYT", new CargoAircraft("A", 6, 7), 1, 1));
        FlightNode flightNode2 = new FlightNode(new Flight("XYT", "XYT", "ABC", new CargoAircraft("A", 6, 7), 1, 1));
        flights.Add(flightNode.Value);
        flights.Add(flightNode1.Value);
        flights.Add(flightNode2.Value);
        Dictionary<string, List<FlightNode>> nodes = [];

        nodes.Add(flightNode.Value.DepartureAirport, []);
        nodes.Add(flightNode1.Value.DepartureAirport, []);
        nodes.Add(flightNode2.Value.DepartureAirport, []);
        nodes["ABC"].Add(new FlightNode(new Flight("ABC", "ABC", "XYZ", new CargoAircraft("A", 6, 7), 1, 1)));
        nodes["XYZ"].Add(new FlightNode(new Flight("XYZ", "XYZ", "XYT", new CargoAircraft("A", 6, 7), 1, 1)));
        nodes["XYT"].Add(new FlightNode(new Flight("XYT", "XYT", "ABC", new CargoAircraft("A", 6, 7), 1, 1)));
        var graph = new Graph(flights);
        graph.Children.Enqueue(flightNode);
        graph.Children.Enqueue(flightNode1);
        graph.Children.Enqueue(flightNode2);
        // Act

        HashSet<FlightNode> flights1 = graph.Traverse("ABC", "XYZ", nodes);
        // Assert
        Assert.Equal(1, flights1.Count);
        // Assuming there is only one path for simplicity
        Assert.Equal("ABC", nodes["ABC"][0].Value.Identifier);
        Assert.Equal("XYZ", nodes["XYZ"][0].Value.Identifier);
        Assert.Equal("XYT", nodes["XYT"][0].Value.Identifier);
    }

    [Fact]
    public void RouteSearchReturnsEmptyQueueWhenNoRouteExists()
    {
        // Arrange
        var flights = new HashSet<Flight>
            {
                new Flight("ABC", "XYZ", "123", new CargoAircraft("azx", 5, 6), 1, 1),
                new Flight("XYZ", "DEF", "456", new CargoAircraft("azx", 5, 6), 1, 1),
                new Flight("XYZ", "GHI", "789", new CargoAircraft("azx", 5, 6), 1, 1)
            };
        var graph = new Graph(flights);

        // Act
        var result = graph.RouteSearch("GHI", "ABC");

        // Assert
        Assert.Empty(result);
    }
}
