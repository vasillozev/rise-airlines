﻿using Airlines.Business.AlgorithmClasses;

namespace Airlines.UnitTests.DataClassesTests
{
    public class SingleLinkedListTests
    {
        [Fact]
        public void AddAddsElementToList()
        {
            // Arrange
            var list = new SingleLinkedList<int>
            {
                // Act
                5
            };

            // Assert
            Assert.Contains(5, list);
        }

        [Fact]
        public void RemoveEndRemovesLastElementFromList()
        {
            // Arrange
            var list = new SingleLinkedList<int>
            {
                1,
                2,
                3
            };

            // Act
            list.RemoveEnd();

            // Assert
            Assert.DoesNotContain(3, list);
        }

        [Fact]
        public void GetEnumeratorReturnsCorrectElements()
        {
            // Arrange
            var list = new SingleLinkedList<string>
            {
                "apple",
                "banana",
                "cherry"
            };

            // Act
            var enumerator = list.GetEnumerator();
            var elements = new List<string>();
            while (enumerator.MoveNext())
                elements.Add(enumerator.Current);

            // Assert
            Assert.Equal(["apple", "banana", "cherry"], elements);
        }

        [Fact]
        public void RemoveEndDoesNothingWhenListIsEmpty()
        {
            // Arrange
            var list = new SingleLinkedList<int>();

            // Act
            list.RemoveEnd();

            // Assert
            Assert.Empty(list);
        }

        [Fact]
        public void RemoveEndRemovesSingleElementWhenListHasOneElement()
        {
            // Arrange
            var list = new SingleLinkedList<int>
            {
                10
            };

            // Act
            list.RemoveEnd();

            // Assert
            Assert.Empty(list);
        }
    }
}
