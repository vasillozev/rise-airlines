﻿using Xunit;
using Airlines.Business.DataClasses;

namespace Airlines.UnitTests.DataClassesTests
{
    public class AirlineTests
    {
        [Fact]
        public void AirlineNameTest()
        {
            // Arrange
            var expectedName = "Test Airline";

            // Act
            var airline = new Airline(expectedName);

            // Assert
            Assert.Equal(expectedName, airline.Name);
        }

        [Fact]
        public void AirlineSetNameTest()
        {
            // Arrange
            var airline = new Airline("Initial Name");
            var expectedName = "Updated Name";

            // Act
            airline.Name = expectedName;

            // Assert
            Assert.Equal(expectedName, airline.Name);
        }
    }
}