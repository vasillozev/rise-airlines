﻿using Airlines.Business.DataClasses;

namespace Airlines.UnitTests.DataClassesTests
{
    public class PrivateAircraftTests
    {
        [Fact]
        public void PrivateAircraftConstructorSetsModelAndSeatsCorrectly()
        {
            // Arrange
            var model = "Cessna 172";
            var seats = 4;

            // Act
            var privateAircraft = new PrivateAircraft(model, seats);

            // Assert
            Assert.Equal(model, privateAircraft.AircraftModel);
            Assert.Equal(seats, privateAircraft.Seats);
        }
    }
}
