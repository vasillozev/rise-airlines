﻿using Xunit;
using Airlines.Business.DataClasses;

namespace Airlines.UnitTests.DataClassesTests
{
    public class PassengerAircraftTests
    {
        [Fact]
        public void PassengerAircraftConstructorSetsPropertiesCorrectly()
        {
            // Arrange
            var model = "Boeing 737";
            var cargoVolume = 100;
            var cargoWeight = 5000;
            var seats = 200;

            // Act
            var passengerAircraft = new PassengerAircraft(model, cargoVolume, cargoWeight, seats);

            // Assert
            Assert.Equal(model, passengerAircraft.AircraftModel);
            Assert.Equal(cargoVolume, passengerAircraft.CargoVolume);
            Assert.Equal(cargoWeight, passengerAircraft.CargoWeight);
            Assert.Equal(seats, passengerAircraft.Seats);
        }
    }
}