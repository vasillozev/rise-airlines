﻿using Airlines.Business.DataClasses;

namespace Airlines.UnitTests.DataClassesTests
{
    public class AirportTests
    {
        [Theory]
        [InlineData("JFK", "New York", "USA")]
        [InlineData("LHR", "London", "UK")]
        [InlineData("CDG", "Paris", "France")]
        public void AirportConstructorTest(string name, string city, string country)
        {
            // Arrange & Act
            var airport = new Airport("IDK", name, city, country);

            // Assert
            Assert.Equal(name, airport.Name);
            Assert.Equal(city, airport.City);
            Assert.Equal(country, airport.Country);
        }

        [Theory]
        [InlineData("JFK")]
        [InlineData("LHR")]
        [InlineData("CDG")]
        public void AirportValidateIdValidTest(string identifier)
        {
            // Act
            var result = Airport.ValidateId(identifier);

            // Assert
            Assert.NotEmpty(result);
        }

        [Theory]
        [InlineData("---")]
        [InlineData("/2")]
        public void AirportValidateIdInvalidTest(string identifier)
        {
            // Act
            var result = Airport.ValidateId(identifier);

            // Assert
            Assert.Empty(result);
        }

        [Theory]
        [InlineData("New York")]
        [InlineData("London")]
        [InlineData("Paris")]
        public void AirportValidateInputValidTest(string input)
        {
            // Act
            var result = Airport.ValidateInput(input);

            // Assert
            Assert.Equal(input, result);
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        [InlineData("123")]
        [InlineData("L0nd0n")]
        [InlineData("Paris!")]
        public void AirportValidateInputInvalidTest(string input)
        {
            // Act
            var result = Airport.ValidateInput(input);

            // Assert
            Assert.Equal(string.Empty, result);
        }
    }
}
