﻿using Airlines.Business.DataClasses;
using Airlines.Business.Exceptions;

namespace Airlines.UnitTests.DataClassesTests
{
    public class FlightTests
    {
        [Fact]
        public void FlightInitialization()
        {
            // Arrange
            var identifier = "ABC123";
            var departureAirport = "JFK";
            var arrivalAirport = "LAX";
            CargoAircraft cargoAircraft = new CargoAircraft("Aerbus", 0.4, 0.4);

            // Act
            var flight = new Flight(identifier, departureAirport, arrivalAirport, cargoAircraft, 1, 1);

            // Assert
            Assert.Equal(identifier, flight.Identifier);
            Assert.Equal(departureAirport, flight.DepartureAirport);
            Assert.Equal(arrivalAirport, flight.ArrivalAirport);
            Assert.Equal("Aerbus", flight.AircraftModel.AircraftModel);

        }

        [Fact]
        public void FlightIdentifierNull()
        {
            // Arrange
            string identifier = null;
            var departureAirport = "JFK";
            var arrivalAirport = "LAX";
            CargoAircraft cargoAircraft = new CargoAircraft("Aerbus", 0.4, 0.4);
            // Act & Assert
            Assert.NotNull(new Flight(identifier, departureAirport, arrivalAirport, cargoAircraft, 1, 1));
        }

        [Fact]
        public void FlightDepartureAirportNull()
        {
            // Arrange
            var identifier = "ABC123";
            string departureAirport = null;
            var arrivalAirport = "LAX";
            CargoAircraft cargoAircraft = new CargoAircraft("Aerbus", 0.4, 0.4);


            // Act & Assert
            Assert.NotNull(new Flight(identifier, departureAirport, arrivalAirport, cargoAircraft, 1, 1));
        }

        [Fact]
        public void FlightArrivalAirportNull()
        {
            // Arrange
            var identifier = "ABC123";
            var departureAirport = "JFK";
            string arrivalAirport = null;
            CargoAircraft cargoAircraft = new CargoAircraft("Aerbus", 0.4, 0.4);

            Flight flight = new Flight(identifier, departureAirport, arrivalAirport, cargoAircraft, 1, 1);
            // Act & Assert
            Assert.NotNull(flight);
        }

        [Fact]
        public void FlightConstructorShouldSetProperties()
        {
            // Arrange
            string identifier = "ABC123";
            string departureAirport = "AAA";
            string arrivalAirport = "BBB";
            IAircraft aircraftModel = new MockAircraft();
            double price = 100.0;
            double timeinHours = 2.5;

            // Act
            Flight flight = new Flight(identifier, departureAirport, arrivalAirport, aircraftModel, price, timeinHours);

            // Assert
            Assert.Equal(identifier, flight.Identifier);
            Assert.Equal(departureAirport, flight.DepartureAirport);
            Assert.Equal(arrivalAirport, flight.ArrivalAirport);
            Assert.Equal(aircraftModel, flight.AircraftModel);
            Assert.Equal(price, flight.Price);
            Assert.Equal(timeinHours, flight.TimeinHours);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData(" ")]
        public void ValidateFlightCodeShouldThrowExceptionForInvalidFlightCode(string flightCode) =>
            // Act & Assert
            Assert.Throws<InvalidFlightCodeException>(() => Flight.ValidateFlightCode(flightCode));

        [Theory]
        [InlineData("ABC123")]
        [InlineData("DEF456")]
        [InlineData("XYZ789")]
        public void ValidateFlightCodeShouldNotThrowExceptionForValidFlightCode(string flightCode) =>
            // Act & Assert
            Assert.True(Flight.ValidateFlightCode(flightCode));
    }

    // Mock IAircraft for testing
    public class MockAircraft : IAircraft
    {
        // Mock IAircraft members
        public string AircraftModel { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public double CargoVolume { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public double CargoWeight { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public int Seats { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
    }
}
