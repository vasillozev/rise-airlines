﻿using Airlines.Business.ConsoleCommands;
using Airlines.Business.DataClasses;
using Airlines.Business.Exceptions;
using Airlines.Business.ReservationBookingSystem;
using Moq;

namespace Airlines.UnitTests.ConsoleCommandsTests
{
    public class ReserveCargoCommandTests
    {
        [Fact]
        public void CreateReserveCargoCommandReturnsInstance()
        {
            // Arrange
            var input = "reserve cargo";
            var setFlights = new HashSet<Flight>();

            // Act
            var result = ReserveCargoCommand.CreateReserveCargoCommand(input, setFlights);

            // Assert
            Assert.NotNull(result);
            _ = Assert.IsType<ReserveCargoCommand>(result);
            Assert.Equal(input, result.Input);
            Assert.Equal(setFlights, result.SetFlights);
        }
        [Fact]
        public void ExecuteReserveCargoCommand()
        {
            // Arrange
            var input = "reserve, cargo, FL123, 0.4";
            var flights = new HashSet<Flight>();
            var reserveCargoCommand = new ReserveCargoCommand(input, flights);
            var mockReservationBookingSystemCommander = new Mock<ReservationBookingSystemCommander>();

            // Assert
            _ = Assert.Throws<InvalidCargoFormatException>(() => reserveCargoCommand.Execute(reserveCargoCommand));
        }
    }
}
