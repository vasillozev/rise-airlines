﻿using Airlines.Business.ConsoleCommands;
using Airlines.Business.DataClasses;
using Airlines.Business.Exceptions;
using Moq;
using System.ComponentModel.Design;

namespace Airlines.UnitTests.ConsoleCommandsTests;
public class DisplayAllAirportsCommandTests
{
    [Fact]
    public void CreateDisplayAirportsCommandReturnsInstanceOfDisplayAllAirportsCommand()
    {
        // Arrange
        var input = "display airports";
        var cityCountryAirports = new Dictionary<string, Airport>();

        // Act
        var result = DisplayAllAirportsCommand.CreateDisplayAirportsCommand(input, cityCountryAirports);

        // Assert
        Assert.NotNull(result);
        _ = Assert.IsType<DisplayAllAirportsCommand>(result);
        Assert.Equal(input, result.Input);
        Assert.Same(cityCountryAirports, result.CityCountryAirports);
    }
    [Fact]
    public void ExecuteDisplayAllAirportsMethodOfCommander()
    {
        // Arrange
        var input = "list, City, Paris";
        var cityCountryAirports = new Dictionary<string, Airport>();
        var displayAllAirportsCommand = new DisplayAllAirportsCommand(input, cityCountryAirports);
        var mockCommander = new Mock<ConsoleCommander>();

        // Act
        displayAllAirportsCommand.Commander = mockCommander.Object;
        ;

        // Assert
        _ = Assert.Throws<InvalidDisplayAllAirportsException>(() => displayAllAirportsCommand.Execute(displayAllAirportsCommand));
    }
}
