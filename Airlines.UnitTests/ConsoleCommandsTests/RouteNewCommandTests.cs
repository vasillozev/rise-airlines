﻿using Airlines.Business.AlgorithmClasses;
using Airlines.Business.ConsoleCommands;
using Airlines.Business.DataClasses;
using Airlines.Business.Exceptions;
using Airlines.Business.ReservationBookingSystem;
using Moq;

namespace Airlines.UnitTests.ConsoleCommandsTests
{
    public class RouteNewCommandTests
    {
        [Fact]
        public void CreateRouteNewCommandReturnsInstance()
        {
            // Arrange
            var input = "route new";
            var flightRoute = new SingleLinkedList<Flight>();
            var setFlights = new HashSet<Flight>();

            // Act
            var result = RouteNewCommand.CreateRouteNewCommand(input, flightRoute, setFlights);

            // Assert
            Assert.NotNull(result);
            _ = Assert.IsType<RouteNewCommand>(result);
            Assert.Equal(input, result.Input);
            Assert.Equal(flightRoute, result.FlightRoute);
            Assert.Equal(setFlights, result.SetFlights);
        }
        [Fact]
        public void ExecuteRouteNewCommand()
        {
            // Arrange
            var input = "route";
            var flightRoute = new SingleLinkedList<Flight>();
            var setFlights = new HashSet<Flight>();
            var routeNewCommand = new RouteNewCommand(input, flightRoute, setFlights);
            var mockReservationBookingSystemCommander = new Mock<ReservationBookingSystemCommander>();

            // Assert
            _ = Assert.Throws<InvalidFlightCodeException>(() => routeNewCommand.Execute(routeNewCommand));
        }
    }
}
