﻿using Xunit;
using Moq;
using Airlines.Business.ConsoleCommands;
using Airlines.Business.DataClasses;
using Airlines.Business.AlgorithmClasses;
using Airlines.Business.Exceptions;
using Airlines.Business.ReservationBookingSystem;

namespace Airlines.UnitTests.ConsoleCommandsTests
{
    public class RouteAddCommandTests
    {
        [Fact]
        public void CreateRouteAddCommandReturnsInstance()
        {
            // Arrange
            var input = "route add";
            var flightRoute = new SingleLinkedList<Flight>();
            var setFlights = new HashSet<Flight>();

            // Act
            var result = RouteAddCommand.CreateRouteAddCommand(input, flightRoute, setFlights);

            // Assert
            Assert.NotNull(result);
            _ = Assert.IsType<RouteAddCommand>(result);
            Assert.Equal(input, result.Input);
            Assert.Equal(flightRoute, result.FlightRoute);
            Assert.Equal(setFlights, result.SetFlights);
        }
        [Fact]
        public void ExecuteRouteAddCommand()
        {
            // Arrange
            var input = "route add";
            var flightRoute = new SingleLinkedList<Flight>();
            var setFlights = new HashSet<Flight>();
            var routeAddCommand = new RouteAddCommand(input, flightRoute, setFlights);
            var mockReservationBookingSystemCommander = new Mock<ReservationBookingSystemCommander>();

            // Assert
            _ = Assert.Throws<InvalidFlightCodeException>(() => routeAddCommand.Execute(routeAddCommand));
        }
    }
}
