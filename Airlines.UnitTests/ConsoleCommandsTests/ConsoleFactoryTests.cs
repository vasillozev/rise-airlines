﻿using Airlines.Business.AlgorithmClasses;
using Airlines.Business.ConsoleCommands;
using Airlines.Business.DataClasses;

public class CommandFactoryTests
{
    [Fact]
    public void ParsingInputReserveTicketInputReturnsReserveTicketCommand()
    {
        // Arrange
        string input = "reserve ticket ABC123";
        var setFlights = new HashSet<Flight>();
        var passengerReservations = new Dictionary<string, Airport>();

        // Act
        var result = CommandFactory.ParsingInput(input, setFlights, passengerReservations, null, null, null);
        CreateCommand(result);
    }

    private static void CreateCommand(IConsoleCommand result)
            // Assert
            => Assert.IsType<ReserveTicketCommand>(result);
    [Fact]
    public void ParsingInputReserveCargoInputReturnsReserveCargoCommand()
    {
        // Arrange
        string input = "reserve cargo XYZ456";
        var setFlights = new HashSet<Flight>();
        var cargoReservations = new Dictionary<string, Airport>();

        // Act
        var result = CommandFactory.ParsingInput(input, setFlights, cargoReservations, null, null, null);

        // Assert
        _ = Assert.IsType<ReserveCargoCommand>(result);
    }

    [Fact]
    public void ParsingInputListInputReturnsDisplayAllAirportsCommand()
    {
        // Arrange
        string input = "list airports";
        var cityCountryAirports = new Dictionary<string, Airport>();

        // Act
        var result = CommandFactory.ParsingInput(input, null, cityCountryAirports, null, null, null);

        // Assert
        _ = Assert.IsType<DisplayAllAirportsCommand>(result);
    }

    // Add more test cases for other command types...

    [Fact]
    public void ParsingInputUnknownInputReturnsDefaultCommand()
    {
        // Arrange
        string input = "unknown command";
        var setFlights = new HashSet<Flight>();

        // Act
        var result = CommandFactory.ParsingInput(input, setFlights, null, null, null, null);

        // Assert
        _ = Assert.IsType<Command>(result);
    }
    [Fact]
    public void ParsingInputListInputReturnsRouteFindCommand()
    {
        // Arrange
        string input = "route find LAX";
        Flight flight = new Flight("FL156", "DFW", "LAX", new CargoAircraft("Boeing 747 - 8F", 140000, 854.5),1,1);
        var flights = new FlightNode(flight);

        // Act
        var result = CommandFactory.ParsingInput(input, null, null, null, null, flights);

        // Assert
        _ = Assert.IsType<FlightRouteSearchCommand>(result);
    }
}
