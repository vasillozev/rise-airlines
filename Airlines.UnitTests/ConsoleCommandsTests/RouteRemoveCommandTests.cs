﻿using Airlines.Business.AlgorithmClasses;
using Airlines.Business.ConsoleCommands;
using Airlines.Business.DataClasses;
using Airlines.Business.Exceptions;
using Airlines.Business.ReservationBookingSystem;
using Moq;

namespace Airlines.UnitTests.ConsoleCommandsTests
{
    public class RouteRemoveCommandTests
    {
        [Fact]
        public void CreateRouteRemoveReturnsInstance()
        {
            // Arrange
            var input = "route remove";
            var flightRoute = new SingleLinkedList<Flight>();
            var setFlights = new HashSet<Flight>();

            // Act
            var result = RouteRemoveCommand.CreateRouteRemoveCommand(input, flightRoute, setFlights);

            // Assert
            Assert.NotNull(result);
            _ = Assert.IsType<RouteRemoveCommand>(result);
            Assert.Equal(input, result.Input);
            Assert.Equal(flightRoute, result.FlightRoute);
            Assert.Equal(setFlights, result.SetFlights);
        }
        [Fact]
        public void ExecuteRouteRemoveCommand()
        {
            // Arrange
            var input = "route remove any";
            var flightRoute = new SingleLinkedList<Flight>();
            var setFlights = new HashSet<Flight>();
            var routeRemoveCommand = new RouteRemoveCommand(input, flightRoute, setFlights);
            var mockReservationBookingSystemCommander = new Mock<ReservationBookingSystemCommander>();

            // Assert
            _ = Assert.Throws<InvalidFlightCodeException>(() => routeRemoveCommand.Execute(routeRemoveCommand));
        }
    }
}
