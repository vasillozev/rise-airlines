﻿using Airlines.Business.ConsoleCommands;
using Airlines.Business.DataClasses;
using Airlines.Business.Exceptions;
using Moq;

namespace Airlines.UnitTests.ConsoleCommandsTests
{
    public class PreciseSearchCommandTests
    {
        [Fact]
        public void CreatePreciseSearchCommandReturnsInstance()
        {
            // Arrange
            var input = "exist";
            var airlines = new HashSet<string>();

            // Act
            var result = PreciseSearchCommand.CreatePreciseSearchCommand(input, airlines);

            // Assert
            Assert.NotNull(result);
            _ = Assert.IsType<PreciseSearchCommand>(result);
            Assert.Equal(input, result.Input);
            Assert.Equal(airlines, result.Airlines);
        }
        [Fact]
        public void ExecutePreciseSearchCommandMethodOfCommander()
        {
            // Arrange
            var input = "exist";
            var airlines = new HashSet<string>();

            // Act
            var preciseSearchCommand = new PreciseSearchCommand(input, airlines);
            var mockCommander = new Mock<ConsoleCommander>();
            // Act
            preciseSearchCommand.Commander = mockCommander.Object;
            ;

            // Assert
            _ = Assert.Throws<InvalidAirlineNameException>(() => preciseSearchCommand.Execute(command: preciseSearchCommand));
        }
    }
}
