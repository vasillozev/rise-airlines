﻿using Xunit;
using Moq;
using Airlines.Business.ConsoleCommands;
using Airlines.Business.DataClasses;
using Airlines.Business.AlgorithmClasses;
using Airlines.Business.Exceptions;
using Airlines.Business.ReservationBookingSystem;

namespace Airlines.UnitTests.ConsoleCommandsTests
{
    public class RoutePrintCommandTests
    {
        [Fact]
        public void CreateRoutePrintCommandReturnsInstance()
        {
            // Arrange
            var flightRoute = new SingleLinkedList<Flight>();

            // Act
            var result = RoutePrintCommand.CreateRoutePrintCommand("input", flightRoute);

            // Assert
            Assert.NotNull(result);
            _ = Assert.IsType<RoutePrintCommand>(result);
            Assert.Equal(flightRoute, result.FlightRoute);
        }
        [Fact]
        public void ExecuteRoutePrintCommand()
        {
            // Arrange
            SingleLinkedList<Flight> flightRoute = null;
            var routePrintCommand = new RoutePrintCommand(flightRoute);
            var mockReservationBookingSystemCommander = new Mock<ReservationBookingSystemCommander>();

            // Assert
            _ = Assert.Throws<InvalidFlightCodeException>(() => routePrintCommand.Execute(routePrintCommand));
        }
    }
}
