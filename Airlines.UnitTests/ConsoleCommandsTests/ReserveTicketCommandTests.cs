﻿using Airlines.Business.ConsoleCommands;
using Airlines.Business.DataClasses;
using Airlines.Business.Exceptions;
using Airlines.Business.ReservationBookingSystem;
using Moq;

namespace Airlines.UnitTests.ConsoleCommandsTests
{
    public class ReserveTicketCommandTests
    {
        [Fact]
        public void CreateReserveTicketCommandReturnsInstance()
        {
            // Arrange
            var input = "reserve ticket";
            var setFlights = new HashSet<Flight>();

            // Act
            var result = ReserveTicketCommand.CreateReserveTicketCommand(input, setFlights);

            // Assert
            Assert.NotNull(result);
            _ = Assert.IsType<ReserveTicketCommand>(result);
            Assert.Equal(input, result.Input);
            Assert.Equal(setFlights, result.SetFlights);
        }
        [Fact]
        public void ExecuteReserveTicketCommand()
        {
            // Arrange
            var input = "reserve, ticket, FL123, 4, 2";
            var flights = new HashSet<Flight>();
            var reserveTicketCommand = new ReserveTicketCommand(input, flights);
            var mockReservationBookingSystemCommander = new Mock<ReservationBookingSystemCommander>();

            // Assert
            _ = Assert.Throws<InvalidReserveTicketFormatException>(() => reserveTicketCommand.Execute(reserveTicketCommand));
        }
    }
}
