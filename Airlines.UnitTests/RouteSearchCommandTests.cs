﻿using Airlines.Business.AlgorithmClasses;
using Airlines.Business.ConsoleCommands;
using Airlines.Business.DataClasses;
using Airlines.Business.Exceptions;
using Airlines.Business.ReservationBookingSystem;
using Moq;

namespace Airlines.UnitTests;
public class RouteSearchCommandTests
{
    [Fact]
    public void CreateFlightRouteSearchCommandReturnsInstanceWithProvidedInputAndFlights()
    {
        // Arrange
        string input = "test_input";
        Flight flight = new Flight("FL123", "JFK", "LAX", new CargoAircraft("Boeing 747 - 8F", 140000, 854.5), 1, 1);
        var flights = new FlightNode(flight);
        var pathAircrafts = Path.Combine("Resources", "aircrafts.csv");
        var aircraftValues = Reader.ReadInput(pathAircrafts);
        HashSet<IAircraft> setAircrafts = ReservationBookingSystemCommander.AircraftListToHashSet(aircraftValues);

        var pathFlights = Path.Combine("Resources", "flights.csv");
        var flightsValues = Reader.ReadInput(pathFlights);
        HashSet<Flight> setFlights = ReservationBookingSystemCommander.FlightListToHashSet(flightsValues, setAircrafts);
        // Act
        var command = FlightRouteSearchCommand.CreateFlightRouteSearchCommand(setFlights, input, flights);

        // Assert
        Assert.NotNull(command);
        _ = Assert.IsType<FlightRouteSearchCommand>(command);
        Assert.Equal(input, command.Input);
        Assert.Equal(flights, command.Flights);
    }
}
