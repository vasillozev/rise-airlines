﻿using Airlines.Persistence.InMemory.Models;

namespace Airlines.Persistence.InMemory.Management;

public class DataManagement(
    List<Airport>? airports,
    List<Airline>? airlines,
    List<Flight>? flights,
    LinkedList<Flight>? route,
    List<CargoAircraft>? cargoAircrafts,
    List<PassengerAircraft>? passengerAircrafts)
{
    private readonly List<Airport>? _airports = airports;
    private readonly List<Airline>? _airlines = airlines;

    public List<Airport>? Airports { get; } = airports;
    public List<Airline>? Airlines { get; } = airlines;
    public List<Flight>? Flights { get; } = flights;
    public LinkedList<Flight>? Routes { get; } = route;
    public List<CargoAircraft>? CargoAircrafts { get; } = cargoAircrafts;
    public List<PassengerAircraft>? PassengerAircrafts { get; } = passengerAircrafts;

    public bool AirlineExists(string airlineName) => _airlines!.Any(airline => airline.Name.Split(' ').Any(name => name.Trim().Equals(airlineName, StringComparison.OrdinalIgnoreCase)));
    private static void PrintSortedData<T>(IEnumerable<T> data)
    {
        Console.WriteLine($"Sorted {typeof(T).Name}:");
        foreach (var item in data)
        {
            Console.WriteLine(item);
        }
    }
}