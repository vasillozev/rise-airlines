﻿namespace Airlines.Persistence.InMemory.Exceptions;
public class MissingFileException(string message) : Exception(message)
{
}
