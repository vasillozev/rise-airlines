﻿namespace Airlines.Persistence.InMemory.Exceptions;
public class InvalidAirportException(string message) : Exception(message)
{
}
