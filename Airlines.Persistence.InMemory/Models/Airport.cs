﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Airlines.Persistence.InMemory.Models
{
    public partial class Airport
    {
        [Key]
        public int AirportId { get; set; }

        [Column("FromAirportID")]
        public int FromAirportId { get; set; }

        [Required]
        [StringLength(256)]
        [Unicode(false)]
        public string? Name { get; set; }

        [Required]
        [StringLength(256)]
        [Unicode(false)]
        public string? Country { get; set; }

        [Required]
        [StringLength(256)]
        [Unicode(false)]
        public string? City { get; set; }

        [Required]
        [StringLength(3)]
        [Unicode(false)]
        public string? Code { get; set; }

        public int RunwaysCount { get; set; }

        public bool Founded { get; set; }
    }
}
