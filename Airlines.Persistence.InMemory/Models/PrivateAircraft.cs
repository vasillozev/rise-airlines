﻿namespace Airlines.Persistence.InMemory.Models;
public class PrivateAircraft : IAircraft
{
    public string AircraftModel { get; set; }
    public double CargoWeight { get; set; }
    public int Seats { get; set; }
    public double CargoVolume { get; set; }

    public PrivateAircraft(string model, int seats)
    {
        AircraftModel = model;
        Seats = seats;
    }
}
