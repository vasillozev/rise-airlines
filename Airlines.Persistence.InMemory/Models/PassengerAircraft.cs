﻿namespace Airlines.Persistence.InMemory.Models;
public class PassengerAircraft : IAircraft
{
    public string AircraftModel { get; set; }
    public double CargoVolume { get; set; }
    public double CargoWeight { get; set; }
    public int Seats { get; set; }
    public PassengerAircraft(string model, double cargoVolume, double cargoWeight, int seats)
    {
        AircraftModel = model;
        CargoVolume = cargoVolume;
        CargoWeight = cargoWeight;
        Seats = seats;
    }
}
