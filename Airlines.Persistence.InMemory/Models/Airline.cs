﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Airlines.Persistence.InMemory.Models
{
    public partial class Airline
    {
        [Key]
        [Column("AirlineID")]
        public int AirlineId { get; set; }

        [Required]
        [StringLength(5)]
        [Unicode(false)]
        public string? Name { get; set; }

        public DateTime Founded { get; set; }

        public int FleetSize { get; set; }

        [Required]
        [StringLength(1000)]
        public string? Description { get; set; }
    }
}
