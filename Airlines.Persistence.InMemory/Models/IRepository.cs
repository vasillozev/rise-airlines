﻿namespace Airlines.Persistence.InMemory.Models;
public interface IRepository<T> where T : class
{
    T? GetById(string id);
    IEnumerable<T> GetAll();
    bool Add(T entity);
    bool Update(T entity);
    bool Delete(string id);
    bool Delete(T entity);
    bool Exists(T entity);
    void DeleteAll();
    int Size { get; }
}
