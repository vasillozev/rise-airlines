﻿namespace Airlines.Persistence.InMemory.Models;
public class CargoAircraft : IAircraft
{
    public string AircraftModel { get; set; }
    public double CargoVolume { get; set; }
    public double CargoWeight { get; set; }
    public int Seats { get; set; }
    public CargoAircraft(string model, double cargoVolume, double cargoWeight)
    {
        AircraftModel = model;
        CargoVolume = cargoVolume;
        CargoWeight = cargoWeight;
    }
}
