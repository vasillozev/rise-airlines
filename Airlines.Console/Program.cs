﻿using Airlines.Business.AlgorithmClasses;
using Airlines.Business.ConsoleCommands;
using Airlines.Business.Exceptions;
using Airlines.Business.Mappers;
using Airlines.Business.ReservationBookingSystem;
using Airlines.Persistence.InMemory.EntitiesRepository;

namespace Airlines.Console
{
    public static class Program
    {
        public static async Task Main()
        {

            var pathAirports = Path.Combine("Resources", "airports.csv");
            var airportsValues = Reader.ReadInput(pathAirports);
            Dictionary<string, Business.DataClasses.Airport> cityCountryAirports = ReservationBookingSystemCommander.AirportListToDictionary(airportsValues);

            var pathAirlines = Path.Combine("Resources", "airlines.csv");
            var airlinesValues = Reader.ReadInput(pathAirlines);
            HashSet<string> airlinesSetNames = ReservationBookingSystemCommander.AirlineListToHashSet(airlinesValues);

            var pathAircrafts = Path.Combine("Resources", "aircrafts.csv");
            var aircraftValues = Reader.ReadInput(pathAircrafts);
            HashSet<Business.DataClasses.IAircraft> setAircrafts = ReservationBookingSystemCommander.AircraftListToHashSet(aircraftValues);
            ConsoleCommander consoleCommander = new ConsoleCommander();

            var pathFlights = Path.Combine("Resources", "flights.csv");
            var flightsValues = Reader.ReadInput(pathFlights);
            HashSet<Business.DataClasses.Flight> setFlights = ReservationBookingSystemCommander.FlightListToHashSet(flightsValues, setAircrafts);
            FlightNode? flightRouteSearch = null;

            try
            {
                var flightRoutes = Path.Combine("Resources", "flightRoutes.csv");
                IList<string[]> strings = Reader.ReadInput(flightRoutes);
                flightRouteSearch = ReservationBookingSystemCommander.FlightRoutesFactory(strings, setFlights);
            }
            catch (InvalidFileExceptionSearchRoute ex)
            {
                System.Console.WriteLine(ex);
            }
            catch (RouteFindException ex)
            {
                System.Console.WriteLine(ex);
            }
            System.Console.WriteLine("Welcome to Airlines application.");

            while (true)
            {
                string? input = System.Console.ReadLine();
                try
                {
                    _ = ConsoleValidator.Validate(input);
                }
                catch (InvalidInputException ex)
                {
                    System.Console.WriteLine(ex);

                    continue;
                }
                if (input == null)
                {
                    continue;
                }
                SingleLinkedList<Business.DataClasses.Flight> flightRoute = [];

                if (input == "batch start")
                {
                    Stack<IConsoleCommand> batch = new Stack<IConsoleCommand>();

                    while (true)
                    {
                        input = System.Console.ReadLine();
                        try
                        {
                            _ = ConsoleValidator.Validate(input);
                        }
                        catch (InvalidInputException ex)
                        {
                            System.Console.WriteLine(ex);
                            continue;
                        }
                        System.Console.WriteLine("Please select command or 'batch cancel' to exit batch mode" +
                            " or 'batch run' to run them simultanously.");
                        if (input == "batch cancel")
                        {
                            System.Console.WriteLine("You have exited batch mode.");
                            batch.Clear();
                            break;
                        }
                        else if (input == "batch run")
                        {
                            while (batch.Count > 0)
                            {
                                IConsoleCommand command = batch.Pop();
                                if (command.GetType() == typeof(RoutePrintCommand))
                                {
                                    try
                                    {
                                        foreach (var flight in flightRoute)
                                        {
                                            System.Console.WriteLine(flight.Identifier);
                                        }
                                    }
                                    catch (InvalidFlightCodeException ex)
                                    {
                                        System.Console.WriteLine(ex);
                                    }
                                }
                                command.Execute(command);
                                continue;
                            }
                        }
                        if (input == null)
                        {
                            continue;
                        }
                        if (input.StartsWith("exist"))
                        {
                            string[] inputValues = input.Split(", ");
                            string airlineName = inputValues[1];
                            try
                            {
                                _ = Business.DataClasses.Airline.ValidateAirline(airlineName);
                            }
                            catch (InvalidAirlineNameException ex)
                            {
                                System.Console.WriteLine(ex);
                                continue;
                            }
                        }
                        else if (input.StartsWith("reserve"))
                        {
                            string[] inputValues = input.Split(" ");
                            string flightCode = inputValues[2];
                            try
                            {
                                _ = Business.DataClasses.Flight.ValidateFlightCode(flightCode);
                            }
                            catch (InvalidFlightCodeException ex)
                            {
                                System.Console.WriteLine(ex);
                                continue;
                            }
                        }
                        try
                        {
                            if (flightRouteSearch != null)
                            {
                                batch.Push(CommandFactory.ParsingInput(input, setFlights, cityCountryAirports,
                                                                        airlinesSetNames, flightRoute, flightRouteSearch));
                            }
                        }

                        catch (InvalidDataException ex)
                        {
                            System.Console.WriteLine(ex);
                            throw;
                        }
                    }
                }
                else if (input.StartsWith("reserve cargo"))
                {
                    System.Console.WriteLine("Please type the command in the format :" +
                            "reserve cargo <Flight Identifier> <Cargo Weight> <Cargo Volume>");
                    string[] commandValues = input.Split(" ");
                    string flightIdentifier = "";
                    try
                    {
                        flightIdentifier = commandValues[2];
                        _ = Business.DataClasses.Flight.ValidateFlightCode(flightIdentifier);
                    }
                    catch (InvalidFlightCodeException ex)
                    {
                        System.Console.WriteLine(ex);
                        continue;
                    }
                    try
                    {
                        ReservationBookingSystemCommander.ReserveCargo(input, setFlights);
                    }
                    catch (InvalidCargoFormatException ex)
                    {
                        System.Console.WriteLine(ex.Message);
                        continue;
                    }
                }
                else if (input.StartsWith("reserve ticket"))
                {
                    System.Console.WriteLine("Please type the command in the format :" +
                           "reserve ticket <Flight Identifier> <Seats> <Small Baggage Count> <Large Baggage Count>");
                    string[] commandValues = input.Split(" ");
                    string flightIdentifier = "";
                    int smallBaggageCount = 0;
                    int largeBaggageCount = 0;
                    Business.DataClasses.Flight? flight = null;
                    try
                    {
                        flightIdentifier = commandValues[2];
                        _ = Business.DataClasses.Flight.ValidateFlightCode(flightIdentifier);
                        flight = setFlights.First(x => x.Identifier == flightIdentifier);
                    }
                    catch (InvalidFlightCodeException ex)
                    {
                        System.Console.WriteLine(ex);
                        continue;
                    }
                    try
                    {
                        ReservationBookingSystemCommander.ReserveTicket(input, setFlights);
                    }
                    catch (InvalidReserveTicketFormatException ex)
                    {
                        System.Console.WriteLine(ex.Message);
                        continue;
                    }
                    for (int i = 0; i < smallBaggageCount; i++)
                    {
                        System.Console.WriteLine("Please select option for small luggage in the format: " +
                            "<Maximum Weight 15 kg>, <Maximum Volume 0.045 m³>.");
                        string? option = System.Console.ReadLine();
                        try
                        {
                            _ = ConsoleValidator.Validate(option);
                        }
                        catch (InvalidInputException ex)
                        {
                            System.Console.WriteLine(ex);
                            continue;
                        }
                        if (option == null)
                        {
                            continue;
                        }
                        string[] optionValues = option.Split(", ");
                        try
                        {
                            _ = double.Parse(optionValues[0]);
                            _ = double.Parse(optionValues[1]);
                        }
                        catch (InvalidSmallOptionException ex)
                        {
                            System.Console.WriteLine(ex.Message);
                            continue;
                        }
                        double smallWeight = double.Parse(optionValues[0]);
                        double smallVolume = double.Parse(optionValues[1]);
                        if (flight == null)
                        {
                            continue;
                        }
                        if (smallWeight > flight.AircraftModel.CargoWeight)
                        {
                            System.Console.WriteLine("There is not enough weight capacity left on the aircraft");
                            continue;
                        }
                        else if (smallVolume > flight.AircraftModel.CargoVolume)
                        {
                            System.Console.WriteLine("There is not enough volume capacity left on the aircraft");
                            continue;
                        }
                        flight.AircraftModel.CargoWeight -= smallWeight;
                        flight.AircraftModel.CargoVolume -= smallVolume;
                    }

                    for (int i = 0; i < largeBaggageCount; i++)
                    {
                        System.Console.WriteLine("Please select option for large luggage in the format: " +
                            "<Maximum Weight 30 kg>, <Maximum Volume 0.090 m³>.");
                        string? option = System.Console.ReadLine();
                        if (option == null)
                        {
                            continue;
                        }
                        string[] optionValues = option.Split(", ");
                        try
                        {
                            _ = double.Parse(optionValues[0]);
                            _ = double.Parse(optionValues[1]);
                        }
                        catch (InvalidLargeOptionException ex)
                        {
                            System.Console.WriteLine(ex.Message);
                            continue;
                        }
                        double largeWeight = double.Parse(optionValues[0]);
                        double largeVolume = double.Parse(optionValues[1]);
                        if (flight == null)
                        {
                            continue;
                        }
                        if (largeWeight > flight.AircraftModel.CargoWeight)
                        {
                            System.Console.WriteLine("There is not enough weight capacity left on the aircraft");
                            continue;
                        }
                        else if (largeVolume > flight.AircraftModel.CargoVolume)
                        {
                            System.Console.WriteLine("There is not enough volume capacity left on the aircraft");
                            continue;
                        }
                        flight.AircraftModel.CargoWeight -= largeWeight;
                        flight.AircraftModel.CargoVolume -= largeVolume;
                    }
                }
                else if (input.StartsWith("route"))
                {
                    string[] inputValues = input.Split(" ");
                    if (inputValues[1] == "check")
                    {
                        Graph graph = new Graph(setFlights);
                        try
                        {
                            if (inputValues.Length != 4)
                            {
                                throw new RouteCheckException();
                            }
                        }
                        catch (RouteCheckException ex)
                        {
                            System.Console.WriteLine($"{ex.Message}");
                        }
                        Dictionary<string, List<FlightNode>> AirportsFlights = [];
                        foreach (var flight in setFlights)
                        {
                            if (!AirportsFlights.ContainsKey(flight.DepartureAirport))
                            {
                                AirportsFlights.Add(flight.DepartureAirport, []);
                            }
                            AirportsFlights[flight.DepartureAirport].Add(new FlightNode(flight));
                        }
                        foreach (var flight in setFlights)
                        {
                            foreach (var listFlightNodes in AirportsFlights.Values)
                            {
                                foreach (var flightNode in listFlightNodes)
                                {
                                    flightNode.AddChild(flight);
                                }
                            }
                        }
                        string startAirport = inputValues[2];
                        string endAirport = inputValues[3];
                        HashSet<FlightNode> flights = graph.Traverse(startAirport, endAirport, AirportsFlights);
                        if (flights.Count > 0)
                        {
                            System.Console.WriteLine("True");
                        }
                        else
                        {
                            System.Console.WriteLine("False");
                        }
                    }

                    if (inputValues[4] == "cheap")
                    {
                        string departureAirport = inputValues[2];
                        string arrivalAirport = inputValues[3];
                        GraphFLights Flights = new GraphFLights();
                        foreach (var flight in setFlights)
                        {
                            Node node = new Node(flight);
                            Flights.Add(node);
                        }
                        foreach (var node in Flights.Nodes)
                        {
                            foreach (var node1 in Flights.Nodes)
                            {
                                if (node.Flight.ArrivalAirport == node1.Flight.DepartureAirport)
                                {
                                    node.AddNeighbour(node1, node1.Flight.Price);
                                }
                            }
                        }
                        PriceCalculator c = new PriceCalculator(Flights);
                        c.Calculate(Flights.Nodes.First(x => x.Flight.DepartureAirport == departureAirport),
                          Flights.Nodes.First(x => x.Flight.DepartureAirport == arrivalAirport));
                    }
                    else if (inputValues[4] == "short")
                    {
                        string departureAirport = inputValues[2];
                        string arrivalAirport = inputValues[3];
                        GraphFLights Flights = new GraphFLights();
                        foreach (var flight in setFlights)
                        {
                            Node node = new Node(flight);
                            Flights.Add(node);
                        }
                        foreach (var node in Flights.Nodes)
                        {
                            foreach (var node1 in Flights.Nodes)
                            {
                                if (node.Flight.ArrivalAirport == node1.Flight.DepartureAirport)
                                {
                                    node.AddNeighbour(node1, node1.Flight.TimeinHours);
                                }
                            }
                        }
                        TimeCalculator c = new TimeCalculator(Flights);
                        c.Calculate(Flights.Nodes.First(x => x.Flight.DepartureAirport == departureAirport),
                          Flights.Nodes.First(x => x.Flight.DepartureAirport == arrivalAirport));
                    }
                    else if (inputValues[4] == "stops")
                    {
                        string departureAirport = inputValues[2];
                        string arrivalAirport = inputValues[3];
                        GraphFLights Flights = new GraphFLights();
                        foreach (var flight in setFlights)
                        {
                            Node node = new Node(flight);
                            Flights.Add(node);
                        }
                        foreach (var node in Flights.Nodes)
                        {
                            foreach (var node1 in Flights.Nodes)
                            {
                                if (node.Flight.ArrivalAirport == node1.Flight.DepartureAirport)
                                {
                                    node.AddNeighbour(node1, node1.Flight.TimeinHours);
                                }
                            }
                        }
                        StopsCalculator c = new StopsCalculator(Flights);
                        c.Calculate(Flights.Nodes.First(x => x.Flight.DepartureAirport == departureAirport),
                          Flights.Nodes.First(x => x.Flight.DepartureAirport == arrivalAirport));
                    }
                    else if (input.StartsWith("route search"))
                    {
                        Graph graph = new Graph(setFlights);
                        string startAirport = inputValues[2];
                        string endAirport = inputValues[3];
                        Dictionary<string, List<FlightNode>> AirportsFlights = [];
                        foreach (var flight in setFlights)
                        {
                            if (!AirportsFlights.ContainsKey(flight.DepartureAirport))
                            {
                                AirportsFlights.Add(flight.DepartureAirport, []);
                            }
                            AirportsFlights[flight.DepartureAirport].Add(new FlightNode(flight));
                        }
                        foreach (var flight in setFlights)
                        {
                            foreach (var listFlightNodes in AirportsFlights.Values)
                            {
                                foreach (var flightNode in listFlightNodes)
                                {
                                    flightNode.AddChild(flight);
                                }
                            }
                        }
                        HashSet<FlightNode> flights = graph.Traverse(startAirport, endAirport, AirportsFlights);
                        foreach (var item in flights)
                        {
                            System.Console.WriteLine(item.Value.Identifier);
                        }
                    }
                    if (inputValues[1] == "find")
                    {
                        if (flightRouteSearch == null)
                        {
                            continue;
                        }
                        try
                        {
                            Queue<FlightNode> flights = ReservationBookingSystemCommander.FlightRouteFind(input, flightRouteSearch);
                            HashSet<string> strings = [];
                            foreach (var item in flights)
                            {
                                _ = strings.Add(item.Value.Identifier);
                            }
                            foreach (var item in strings)
                            {
                                System.Console.WriteLine(item);
                            }
                        }
                        catch (RouteFindException ex)
                        {
                            System.Console.WriteLine(ex.Message);
                            throw;
                        }
                    }
                }
                else if (input.StartsWith("route new"))
                {

                    flightRoute = consoleCommander.RouteNew(input);
                }
                else if (input.StartsWith("route print"))
                {
                    foreach (var flight in flightRoute)
                    {
                        System.Console.WriteLine(flight.Identifier);
                    }
                    continue;
                }
                else if (input.StartsWith("route add"))
                {
                    try
                    {
                        string flightIdentifier = input.Split(' ')[2];
                        Business.DataClasses.Flight flight = setFlights.First(x => x.Identifier == flightIdentifier);
                    }
                    catch (InvalidFlightCodeException ex)
                    {
                        System.Console.WriteLine(ex);
                    }
                    flightRoute = consoleCommander.FlightsCommand(input, flightRoute, setFlights);
                }
                else if (input.StartsWith("route remove"))
                {
                    flightRoute = consoleCommander.FlightsCommand(input, flightRoute, setFlights);
                }
                else if (input.StartsWith("exist"))
                {
                    try
                    {
                        _ = consoleCommander.PreciseSearch(input, airlinesSetNames);
                    }
                    catch (InvalidAirlineNameException ex)
                    {
                        System.Console.WriteLine($"{ex.Message}");
                        continue;
                    }
                    if (consoleCommander.PreciseSearch(input, airlinesSetNames))
                    {
                        System.Console.WriteLine("True");
                    }
                    else
                    {
                        System.Console.WriteLine("False");
                    }
                }
                else if (input.StartsWith("list"))
                {
                    string[] inputValues = input.Split(" ");
                    string? inputData = string.Empty;
                    try
                    {
                        inputData = System.Console.ReadLine();
                    }
                    catch (InvalidInputException ex)
                    {
                        System.Console.WriteLine(ex);
                    }
                    if (!ConsoleValidator.Validate(inputData))
                    {
                        continue;
                    }
                    if (inputData == null)
                    {
                        continue;
                    }
                    string data = "";
                    string from = "";
                    try
                    {
                        _ = ConsoleValidator.Validate(inputValues[1]);
                        _ = ConsoleValidator.Validate(inputValues[2]);

                    }
                    catch (InvalidDataException ex)
                    {
                        System.Console.WriteLine(ex);
                        continue;
                    }
                    if (from is not "City" and not "Country")
                    {
                        continue;
                    }
                    data = inputValues[1];
                    from = inputValues[2];
                    try
                    {
                        foreach (var airport in consoleCommander.DisplayAllAirports(inputData, cityCountryAirports))
                        {
                            System.Console.WriteLine(airport);
                        }
                    }
                    catch (InvalidDisplayAllAirportsException ex)
                    {
                        System.Console.WriteLine(ex);
                        continue;
                    }
                }
                if (input == "exit")
                {
                    break;
                }
            }
        }
    }
}

