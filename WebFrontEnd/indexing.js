document.addEventListener("DOMContentLoaded", async () => {
  const flightData = await getAllAsync("flights");
  const airlineData = await getAllAsync("airlines");
  const airportData = await getAllAsync("airports");

  if (flightData) {
    const flightCount = document.getElementById("flight-count");
    flightCount.innerHTML = flightData.length;
  }
  if (airportData) {
    const airportCount = document.getElementById("airport-count");
    airportCount.innerHTML = airportData.length;
  }
  if (airlineData) {
    const airlineCount = document.getElementById("airline-count");
    airlineCount.innerHTML = airlineData.length;
  }
});
