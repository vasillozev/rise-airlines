const type = "airports";
let form;
let inputAirportName;
let inputAirportFounded;
let inputAirportFleetSize;
let inputAirportDescription;

async function showAirports() {
  const airportData = await getAllAsync(type);

  if (airportData) {
    const tableBody = document.getElementById("airports-tbody");
    tableBody.innerHTML = "";

    airportData.forEach((airport) => {
      const row = document.createElement("tr");
      row.setAttribute("data-id", airport.AirportId);
      console.log(airport);

      row.innerHTML = `
             <td>${airport.name}</td>
                <td>${airport.country}</td>
                <td>${airport.city}</td>
                <td>${airport.code}</td>
                <td>${airport.runways}</td>
                <td>${new Date(airport.founded).toLocaleDateString()}</td>
                <td>
                    <a><button class="edit-btn">Edit</button></a>
                    <a><button class="delete-btn">Delete</button></a>
                </td>
            `;

      row.querySelector(".delete-btn").addEventListener("click", async () => {
        const success = await deleteAsync(type, airport.airportId);
        if (success) {
          tableBody.removeChild(row);
        } else {
          console.error("Failed to delete the airport.");
        }
      });

      row.querySelector(".edit-btn").addEventListener("click", async () => {
        const data = await getOneAsync(type, airport.airportId);
        console.log(data);
        form.setAttribute("data-id", data.airportId);
        inputAirportId = data.airportId;
        inputAirportName.value = data.name;
        inputAirportCountry.value = data.Country;
        inputAirportCity.value = data.city;
        inputAirportCode.value = data.code;
        inputAirportRunways.value = data.runways;
        inputAirportFounded.value = data.founded;
      });
      tableBody.appendChild(row);
    });
  }
}

document.addEventListener("DOMContentLoaded", async () => {
  form = document.getElementById("form");
  inputAirportName = document.getElementById("airport-name");
  inputAirportCountry = document.getElementById("airport-country");
  inputAirportCity = document.getElementById("airport-city");
  inputAirportCode = document.getElementById("airport-code");
  inputAirportRunways = document.getElementById("airport-runways");
  inputAirportFounded = document.getElementById("airport-founded");

  showAirports();

  form.addEventListener("submit", async (e) => {
    e.preventDefault();
    let data = {
      name: inputAirportName.value,
      country: inputAirportCountry.value,
      city: inputAirportCity.value,
      code: inputAirportCode.value,
      runways: inputAirportRunways.value,
      founded: inputAirportFounded.value,
    };
    let id = form.getAttribute("data-id");
    if (id) {
      data.airportId = id;
      let result = await update(type, data);
      console.log(result);
    } else {
      let result = await add(type, data);
      console.log(result);
    }
    showAirports();
    form.reset();
    form.setAttribute("data-id", "");

    return false;
  });
});
