const baseUrl = "http://localhost:5119/api/";

async function getAllAsync(type) {
  try {
    const url = `${baseUrl}${type}`;
    const response = await fetch(url);
    if (!response.ok) {
      throw new Error(`Could not fetch resource: ${response.statusText}`);
    }
    const data = await response.json();
    return data;
  } catch (error) {
    console.error(`Error fetching ${type} data:`, error);
    return null;
  }
}

async function getOneAsync(type, id) {
  try {
    const url = `${baseUrl}${type}/${id}`;
    const response = await fetch(url);
    if (!response.ok) {
      throw new Error(`Could not fetch resource: ${response.statusText}`);
    }
    const data = await response.json();
    return data;
  } catch (error) {
    console.error(`Error fetching ${type} with ID ${id}:`, error);
    return null;
  }
}

async function add(type, entity) {
  try {
    const response = await fetch(baseUrl + type, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(entity),
    });

    if (!response.ok) {
      throw new Error("Failed to add");
    }

    return await response.json();
  } catch (error) {
    console.error("Error adding entity:", error);
  }
}

async function update(type, entity) {
  try {
    const response = await fetch(`${baseUrl}${type}`, {
      method: "PUT",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(entity),
    });

    if (!response.ok) {
      throw new Error("Failed to update");
    }

    return await response.json();
  } catch (error) {
    console.error("Error updating entity:", error);
  }
}

async function deleteAsync(type, id) {
  try {
    const url = `${baseUrl}${type}/${id}`;
    const response = await fetch(url, {
      method: "DELETE",
    });
    if (!response.ok) {
      throw new Error(
        `Could not delete ${type} with ID ${id}: ${response.statusText}`
      );
    }
    const data = await response.json();
    return data;
  } catch (error) {
    console.error(`Error deleting ${type} with ID ${id}:`, error);
    return null;
  }
}
