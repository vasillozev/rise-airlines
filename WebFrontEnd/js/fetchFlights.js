const type = "flights";
let form;
let inputFlightNumber;
let inputDepAirport;
let inputArrAirport;
let inputDepDatetime;
let inputArrDatetime;
let airports;
async function showFlights() {
  const flightData = await getAllAsync(type);

  if (!flightData) {
    console.error("Failed to fetch flight data.");
    return;
  }
  const tableBody = document.getElementById("flights-tbody");
  tableBody.innerHTML = "";

  flightData.forEach((flight) => {
    const row = document.createElement("tr");
    row.setAttribute("data-id", flight.airportId);
    console.log(flight);
    row.innerHTML = `
            <td>${flight.flightNumber}</td>
            <td>${flight.fromAirport.name}</td>
            <td>${flight.toAirport.name}</td>
            <td>${new Date(flight.departureDateTime).toLocaleString()}</td>
            <td>${new Date(flight.arrivalDateTime).toLocaleString()}</td>
            <td>
                <a><button class="edit-btn">Edit</button></a>
                <a><button class="delete-btn">Delete</button></a>
            </td>
        `;

    row.querySelector(".delete-btn").addEventListener("click", async () => {
      const success = await deleteAsync(type, flight.flightId);
      if (success) {
        tableBody.removeChild(row);
      } else {
        console.error("Failed to delete the flight.");
      }
    });

    row.querySelector(".edit-btn").addEventListener("click", async () => {
      const data = await getOneAsync(type, flight.flightId);
      const departureAirport = airports.find(
        (a) => a.airportId == data.fromAirportId
      );
      const arrivalAirport = airports.find(
        (a) => a.airportId == data.toAirportId
      );
      console.log(data);

      form.setAttribute("data-id", data.flightId);
      inputFlightNumber.value = data.flightNumber;
      inputDepAirport.value = departureAirport.name;
      inputArrAirport.value = arrivalAirport.name;
      inputDepDatetime.value = data.departureDateTime;
      inputArrDatetime.value = data.arrivalDateTime;
    });

    tableBody.appendChild(row);
  });
}

document.addEventListener("DOMContentLoaded", async () => {
  form = document.getElementById("form");
  inputFlightNumber = document.getElementById("flight-number");
  inputDepAirport = document.getElementById("departure-airport");
  inputArrAirport = document.getElementById("arrival-airport");
  inputDepDatetime = document.getElementById("departure-datetime");
  inputArrDatetime = document.getElementById("arrival-datetime");

  airports = await getAllAsync("airports");

  showFlights();

  form.addEventListener("submit", async (e) => {
    e.preventDefault();
    let depAirport = airports.find((a) => a.name == inputDepAirport.value);
    if (!depAirport) {
      console.error(
        "Invalid departure airport selected",
        inputDepAirport.value
      );
      return false;
    }
    let arrAirport = airports.find((a) => a.name == inputArrAirport.value);
    if (!arrAirport) {
      console.error("Invalid arrival airport selected", inputArrAirport.value);
      return false;
    }
    let data = {
      flightNumber: inputFlightNumber.value,
      fromAirportId: depAirport.airportId,
      toAirportId: arrAirport.airportId,
      departureDateTime: inputDepDatetime.value,
      arrivalDateTime: inputArrDatetime.value,
    };
    let id = form.getAttribute("data-id");
    if (id) {
      data.flightId = id;
      let result = await update(type, data);
      console.log(result);
    } else {
      let result = await add(type, data);
      console.log(result);
    }
    showFlights();
    form.reset();
    form.setAttribute("data-id", "");

    return false;
  });
});
