const type = "airlines";
let form;
let inputAirlineName;
let inputAirlineFounded;
let inputAirlineFleetSize;
let inputAirlineDescription;

async function showAirlines() {
  const airlineData = await getAllAsync(type);

  if (airlineData) {
    const tableBody = document.getElementById("airlines-tbody");
    tableBody.innerHTML = "";

    airlineData.forEach((airline) => {
      const row = document.createElement("tr");
      row.setAttribute("data-id", airline.AirlineId);
      console.log(airline);

      row.innerHTML = `
              <td>${airline.name}</td>
              <td>${new Date(airline.founded).toLocaleDateString()}</td>
              <td>${airline.fleet_size}</td>
              <td>${airline.description}</td>
              <td>
                  <a><button class="edit-btn">Edit</button></a>
                  <a><button class="delete-btn">Delete</button></a>
              </td>
          `;

      row.querySelector(".delete-btn").addEventListener("click", async () => {
        const success = await deleteAsync(type, airline.airlineId);
        if (success) {
          tableBody.removeChild(row);
        } else {
          console.error("Failed to delete the airline.");
        }
      });

      row.querySelector(".edit-btn").addEventListener("click", async () => {
        const data = await getOneAsync(type, airline.airlineId);
        console.log(data);
        form.setAttribute("data-id", data.airlineId);
        inputAirlineId = data.airlineId;
        inputAirlineName.value = data.name;
        inputAirlineFounded.value = data.founded;
        inputAirlineFleetSize.value = data.fleet_size;
        inputAirlineDescription.value = data.description;
      });
      tableBody.appendChild(row);
    });
  }
}

document.addEventListener("DOMContentLoaded", async () => {
  form = document.getElementById("form");
  inputAirlineName = document.getElementById("airline-name");
  inputAirlineFounded = document.getElementById("airline-founded");
  inputAirlineFleetSize = document.getElementById("airline-fleet-size");
  inputAirlineDescription = document.getElementById("airline-description");

  showAirlines();

  form.addEventListener("submit", async (e) => {
    e.preventDefault();
    let data = {
      name: inputAirlineName.value,
      founded: inputAirlineFounded.value,
      fleetSize: inputAirlineFleetSize.value,
      description: inputAirlineDescription.value,
    };
    let id = form.getAttribute("data-id");
    if (id) {
      data.airlineId = id;
      let result = await update(type, data);
      console.log(result);
    } else {
      let result = await add(type, data);
      console.log(result);
    }
    showAirlines();
    form.reset();
    form.setAttribute("data-id", "");

    return false;
  });
});
