﻿using Airlines.Business.DtoModels;
using Airlines.Business.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Airlines.Web.Api.Controllers;

[ApiController]
[Route("api/[controller]")]
public class AirportsController : ControllerBase
{
    private readonly IAirportService _airportService;

    public AirportsController(IAirportService airportService) => _airportService = airportService;

    [HttpGet("{id}")]
    public async Task<ActionResult<AirportDTO>> GetOne(int id)
    {
        var airport = await _airportService.GetAirportByIdAsync(id);

        if (airport == null)
        {
            return NotFound();
        }

        return Ok(airport);
    }

    [HttpGet]
    public async Task<ActionResult<List<AirportDTO>>> GetAll()
    {
        var airports = await _airportService.GetAllAirportsAsync();

        if (airports == null)
        {
            return NotFound();
        }

        return Ok(airports);
    }

    [HttpGet("count")]
    public async Task<ActionResult<int>> GetCount()
    {
        var count = await _airportService.GetAirportsCountAsync();
        return Ok(count);
    }

    [HttpPost]
    public async Task<ActionResult<AirportDTO>> Create([FromBody] AirportDTO AirportDTO)
    {
        if (!ModelState.IsValid)
        {
            return BadRequest(ModelState);
        }

        var airport = await _airportService.AddAirportAsync(AirportDTO);

        return CreatedAtAction("Create", airport);
    }

    [HttpPut]
    public async Task<ActionResult<AirportDTO>> Update([FromBody] AirportDTO AirportDTO)
    {
        if (!ModelState.IsValid)
        {
            return BadRequest(ModelState);
        }

        var airport = await _airportService.UpdateAirportAsync(AirportDTO);

        if (airport == null)
        {
            return NotFound();
        }

        return Ok(airport);
    }

    [HttpPut("{id}")]
    public async Task<ActionResult<AirportDTO>> Update(int id, [FromBody] AirportDTO AirportDTO)
    {
        if (!ModelState.IsValid)
        {
            return BadRequest(ModelState);
        }

        var airport = await _airportService.UpdateAirportAsync(id, AirportDTO);

        if (airport == null)
        {
            return NotFound();
        }

        return Ok(airport);
    }

    [HttpDelete("{id}")]
    public async Task<ActionResult<AirportDTO>> Delete(int id)
    {
        var airport = await _airportService.DeleteAirportAsync(id);

        if (airport == null)
        {
            return NotFound();
        }

        return NoContent();
    }
}