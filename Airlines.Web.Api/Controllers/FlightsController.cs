﻿using Airlines.Business.DtoModels;
using Airlines.Business.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace Airlines.Web.Api.Controllers;

[ApiController]
[Route("api/[controller]")]
public class FlightsController(IFlightService flightService) : Controller
{
    private readonly IFlightService _flightService = flightService;

    [HttpGet]
    public async Task<ActionResult<List<FlightDTO>>> GetAll()
    {
        try
        {
            var flights = await _flightService.GetAllFlightsAsync();
            if (flights?.Count == 0)
                return NoContent();
            return Ok(flights);
        }
        catch (Exception)
        {
            return StatusCode(500, new { message = "Internal server error." });
        }
    }

    [HttpGet("{id}")]
    public async Task<ActionResult<FlightDTO>> GetOne(int id)
    {
        try
        {
            var flight = await _flightService.GetFlightByIdAsync(id);
            if (flight == null)
                return NotFound();
            return Ok(flight);
        }
        catch (Exception)
        {
            return StatusCode(500, new { message = "Internal server error." });
        }
    }

    [HttpPost]
    public async Task<ActionResult<FlightDTO>> Create([FromBody] FlightDTO flightDTO)
    {
        if (!ModelState.IsValid)
            return BadRequest(ModelState);
        try
        {
            var flight = await _flightService.AddFlightAsync(flightDTO);
            if (flight == null)
                return BadRequest();
            return CreatedAtAction("Create", flight);
        }
        catch (Exception)
        {
            return StatusCode(500, new { message = "Internal server error." });
        }
    }

    [HttpPut]
    public async Task<ActionResult<FlightDTO>> Update([FromBody] FlightDTO flightDTO)
    {
        if (!ModelState.IsValid)
            return BadRequest(ModelState);
        try
        {
            var flight = await _flightService.UpdateFlightAsync(flightDTO);
            if (flight == null)
                return BadRequest();
            return Ok(flight);
        }
        catch (Exception)
        {
            return StatusCode(500, new { message = "Internal server error." });
        }
    }

    [HttpDelete("{id}")]
    public async Task<ActionResult<FlightDTO>> Delete(int id)
    {
        try
        {
            var flight = await _flightService.DeleteFlightAsync(id);
            if (flight == null)
                return NotFound();
            return Ok(flight);
        }
        catch (Exception)
        {
            return StatusCode(500, new { message = "Internal server error." });
        }
    }
}
