﻿using Airlines.Business.AlgorithmClasses;
using Airlines.Business.DataClasses;

namespace Airlines.Business.ConsoleCommands;
public class RouteAddCommand : IConsoleCommand
{
    private readonly ConsoleCommander _commander = new();
    public string Input { get; set; }
    internal SingleLinkedList<Flight> FlightRoute { get; set; }
    public HashSet<Flight> SetFlights { get; set; }
    public RouteAddCommand(string input, SingleLinkedList<Flight> flightRoute, HashSet<Flight> setFlights)
    {
        Input = input;
        FlightRoute = flightRoute;
        SetFlights = setFlights;
    }
    public void Execute(IConsoleCommand command) => _commander.FlightsCommand(Input, FlightRoute, SetFlights);
    internal static RouteAddCommand CreateRouteAddCommand(string input, SingleLinkedList<Flight> flightRoute, HashSet<Flight> setFlights) => new(input, flightRoute, setFlights);
}
