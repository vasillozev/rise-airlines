﻿using Airlines.Business.AlgorithmClasses;
using Airlines.Business.DataClasses;

namespace Airlines.Business.ConsoleCommands;
public class RouteNewCommand : IConsoleCommand
{
    private readonly ConsoleCommander _commander = new();
    public string Input { get; set; }
    public SingleLinkedList<Flight> FlightRoute { get; set; }
    public HashSet<Flight> SetFlights { get; set; }
    public RouteNewCommand(string input, SingleLinkedList<Flight> flightRoute, HashSet<Flight> setFlights)
    {
        Input = input;
        FlightRoute = flightRoute;
        SetFlights = setFlights;
    }
    public void Execute(IConsoleCommand command) => _commander.RouteNew(Input);

    internal static RouteNewCommand CreateRouteNewCommand(string input, SingleLinkedList<Flight> flightRoute, HashSet<Flight> setFlights) => new(input, flightRoute, setFlights);
}
