﻿using Airlines.Business.AlgorithmClasses;
using Airlines.Business.DataClasses;
using Airlines.Business.ReservationBookingSystem;

namespace Airlines.Business.ConsoleCommands;
public class ReserveTicketCommand : IConsoleCommand
{
    public string Input { get; set; }
    public HashSet<Flight> SetFlights { get; set; }
    public ReserveTicketCommand(string input, HashSet<Flight> setFlights)
    {
        Input = input;
        SetFlights = setFlights;
    }
    public void Execute(IConsoleCommand command) => ReservationBookingSystemCommander.ReserveTicket(Input, SetFlights);
    internal static ReserveTicketCommand CreateReserveTicketCommand(string input, HashSet<Flight> setFlights) => new(input, setFlights);

}
