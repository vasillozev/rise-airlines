﻿namespace Airlines.Business.ConsoleCommands;
public interface IConsoleCommand
{
    public void Execute(IConsoleCommand command);
}
