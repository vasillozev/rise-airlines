﻿using Airlines.Business.AlgorithmClasses;
using Airlines.Business.DataClasses;

namespace Airlines.Business.ConsoleCommands;
public class RoutePrintCommand : IConsoleCommand
{
    private readonly ConsoleCommander _commander = new();
    public SingleLinkedList<Flight> FlightRoute { get; set; }
    public RoutePrintCommand(SingleLinkedList<Flight> flightRoute) => FlightRoute = flightRoute;
    public void Execute(IConsoleCommand command) => _commander.RoutePrint(FlightRoute);

    internal static RoutePrintCommand CreateRoutePrintCommand(string input, SingleLinkedList<Flight> flightRoute) => new(flightRoute);
}
