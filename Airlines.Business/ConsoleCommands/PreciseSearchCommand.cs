﻿namespace Airlines.Business.ConsoleCommands;
public class PreciseSearchCommand : IConsoleCommand
{
    public ConsoleCommander Commander { get; set; } = new ConsoleCommander();
    public string Input { get; set; }
    public HashSet<string> Airlines { get; set; }
    public PreciseSearchCommand(string input, HashSet<string> airlines)
    {
        Input = input;
        Airlines = airlines;
    }
    public void Execute(IConsoleCommand command) => Commander.PreciseSearch(Input, Airlines);

    internal static PreciseSearchCommand CreatePreciseSearchCommand(string input, HashSet<string> airlines) => new(input, airlines);
}
