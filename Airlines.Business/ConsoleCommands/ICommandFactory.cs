﻿using System.Windows.Input;

namespace Airlines.Business.ConsoleCommands;
public interface ICommandFactory
{
    internal IConsoleCommand CreateCommand();
}
