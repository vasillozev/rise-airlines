﻿using Airlines.Business.AlgorithmClasses;
using Airlines.Business.DataClasses;
using Airlines.Business.Exceptions;

namespace Airlines.Business.ConsoleCommands;
public class ConsoleCommander
{
    /// <summary>
    /// Function for precise search of airline
    /// </summary>
    /// <param name="searchTerm"></param>
    /// <param name="airlines"></param>
    /// <returns></returns>
    internal bool PreciseSearch(string input, HashSet<string> airlines)
    {
        string[] inputValues = input.Split(" ");
        if (inputValues.Length != 2 || !Airline.ValidateAirline(input.Split(" ")[1]))
        {
            throw new InvalidAirlineNameException();
        }
        var airlineName = inputValues[1];
        if (airlines.Contains(airlineName))
        {
            return true;
        }
        return false;
    }
    internal string[] DisplayAllAirports(string input, Dictionary<string, Airport> cityCountryAirports)
    {
        string[] inputValues = input.Split(", ");
        string inputData = inputValues[1];
        string from = inputValues[2];

        if (from is not "City" and not "Country")
        {
            throw new InvalidDisplayAllAirportsException();
        }
        if (from == "City")
        {
            return cityCountryAirports.Where(x => x.Value.City == inputData).Select(x => x.Key).ToArray();
        }
        return cityCountryAirports.Where(x => x.Value.Country == inputData).Select(x => x.Key).ToArray();
    }

    internal SingleLinkedList<Flight> FlightsCommand(string inputMessage, SingleLinkedList<Flight> flightRoute, HashSet<Flight> setFlights)
    {
        string[] inputValues = inputMessage.Split(' ');
        if (inputMessage.StartsWith("route add"))
        {
            if (inputValues.Length != 3)
            {
                throw new InvalidFlightCodeException();
            }
            string flightIdentifier = inputValues[2] ?? throw new InvalidFlightCodeException();
            Flight? flight = setFlights.FirstOrDefault(x => x.Identifier == flightIdentifier);
            if (flight != null)
            {
                if (!flightRoute.Any())
                {
                    flightRoute.Add(setFlights.First(x => x.Identifier == flightIdentifier));
                }
                else if (setFlights.First(x => x.Identifier == flightIdentifier).DepartureAirport
                    == flightRoute.Last().ArrivalAirport)
                {
                    flightRoute.Add(setFlights.First(x => x.Identifier == flightIdentifier));
                }
            }
        }
        else if (inputMessage.StartsWith("route remove"))
        {
            if (inputValues.Length != 2)
            {
                throw new InvalidFlightCodeException();
            }
            flightRoute.RemoveEnd();
        }
        return flightRoute;
    }
    internal List<string> RoutePrint(SingleLinkedList<Flight> flights)
    {
        if (flights == null)
        {
            throw new InvalidFlightCodeException();
        }
        List<string> flightNames = [];
        foreach (var flight in flights)
        {
            flightNames.Add(flight.Identifier);
        }
        return flightNames;
    }
    internal SingleLinkedList<Flight> RouteNew(string input)
    {
        if (input.Split(' ').Length != 2 || input.Split(' ')[1] != "new")
        {
            throw new InvalidFlightCodeException();
        }
        return [];
    }
}


