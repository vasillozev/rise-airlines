﻿using Airlines.Business.DataClasses;
using Airlines.Business.ReservationBookingSystem;

namespace Airlines.Business.ConsoleCommands;
public class ReserveCargoCommand : IConsoleCommand
{
    public string Input { get; set; }
    public HashSet<Flight> SetFlights { get; set; }
    public ReserveCargoCommand(string input, HashSet<Flight> setFlights)
    {
        Input = input;
        SetFlights = setFlights;
    }
    public void Execute(IConsoleCommand command) => ReservationBookingSystemCommander.ReserveCargo(Input, SetFlights);
    internal static ReserveCargoCommand CreateReserveCargoCommand(string input, HashSet<Flight> setFlights) => new(input, setFlights);

}
