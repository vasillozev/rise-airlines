﻿using Airlines.Business.AlgorithmClasses;
using Airlines.Business.DataClasses;
using Airlines.Business.ReservationBookingSystem;

namespace Airlines.Business.ConsoleCommands;
internal class FlightRouteSearchCommand : IConsoleCommand
{
    public string Input { get; set; }
    public FlightNode Flights { get; set; }
    public HashSet<Flight> Flights1 { get; set; }
    public FlightRouteSearchCommand(HashSet<Flight> flights1, string input, FlightNode flights)
    {
        Flights1 = flights1;
        Input = input;
        Flights = flights;
    }
    public void Execute(IConsoleCommand command) => ReservationBookingSystemCommander.FlightRouteFind(Input, Flights);
    internal static FlightRouteSearchCommand CreateFlightRouteSearchCommand(HashSet<Flight> flights1, string input, FlightNode flights) => new(flights1, input, flights);
}
