﻿using Airlines.Business.DataClasses;

namespace Airlines.Business.ConsoleCommands;
public class DisplayAllAirportsCommand : IConsoleCommand
{
    public ConsoleCommander Commander { get; set; } = new ConsoleCommander();
    public string Input { get; set; }
    public Dictionary<string, Airport> CityCountryAirports { get; set; }
    public DisplayAllAirportsCommand(string input, Dictionary<string, Airport> cityCountryAirports)
    {
        Input = input;
        CityCountryAirports = cityCountryAirports;
    }
    public void Execute(IConsoleCommand command) => Commander.DisplayAllAirports(Input, CityCountryAirports);

    internal static DisplayAllAirportsCommand CreateDisplayAirportsCommand(string input, Dictionary<string, Airport> cityCountryAirports) => new(input, cityCountryAirports);
}
