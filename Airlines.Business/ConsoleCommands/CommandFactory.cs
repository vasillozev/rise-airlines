﻿using Airlines.Business.AlgorithmClasses;
using Airlines.Business.DataClasses;

namespace Airlines.Business.ConsoleCommands;
public static class CommandFactory
{
    internal static IConsoleCommand ParsingInput(string input, HashSet<Flight> setFlights,
        Dictionary<string, Airport> cityCountryAirports, HashSet<string> airlines,
        SingleLinkedList<Flight> flightRoute, FlightNode flights)
    {
        string[] inputValues = input.Split(' ');
        string inputValue = inputValues[0];
        switch (inputValue)
        {
            case "reserve":
                if (inputValues[1] == "ticket")
                {
                    return ReserveTicketCommand.CreateReserveTicketCommand(input, setFlights);
                }
                else
                {
                    return ReserveCargoCommand.CreateReserveCargoCommand(input, setFlights);
                }
            case "list":
                return DisplayAllAirportsCommand.CreateDisplayAirportsCommand(input, cityCountryAirports);
            case "exist":
                return PreciseSearchCommand.CreatePreciseSearchCommand(input, airlines);
            case "route":
                if (inputValues[1] == "add")
                {
                    return RouteAddCommand.CreateRouteAddCommand(input, flightRoute, setFlights);
                }
                else if (inputValues[1] == "new")
                {
                    return RouteNewCommand.CreateRouteNewCommand(input, flightRoute, setFlights);
                }
                else if (inputValues[1] == "remove")
                {
                    return RouteRemoveCommand.CreateRouteRemoveCommand(input, flightRoute, setFlights);
                }
                else if (inputValues[1] == "print")
                {
                    return RoutePrintCommand.CreateRoutePrintCommand(input, flightRoute);
                }
                else
                {
                    return FlightRouteSearchCommand.CreateFlightRouteSearchCommand(setFlights,input, flights);
                }
            default: return new Command();

        }
    }
}
