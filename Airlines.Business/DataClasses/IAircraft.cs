﻿namespace Airlines.Business.DataClasses;
public interface IAircraft
{
    public string AircraftModel { get; set; }
    public double CargoVolume { get; set; }
    public double CargoWeight { get; set; }
    public int Seats { get; set; }
}
