﻿namespace Airlines.Business.DataClasses;
public class Airport(string id, string name, string city, string country)
{
    private string _id = id;
    private string _name = name;
    private string _city = city;
    private string _country = country;

    public string Id
    {
        get => _id;
        set => _id = ValidateId(value);
    }
    public string Name
    {
        get => _name;
        set => _name = ValidateInput(value);
    }
    public string City
    {
        get => _city;
        set => _city = ValidateInput(value);
    }
    public string Country
    {
        get => _country;
        set => _country = ValidateInput(value);
    }

    internal static string ValidateInput(string input)
    {
        if (string.IsNullOrWhiteSpace(input))
        {
            input = string.Empty;
            return input;
        }
        else if (input == string.Empty)
        {
            return input;
        }
        else
        {
            foreach (var letter in input)
                if (!char.IsLetter(letter) && letter != ' ')
                {
                    input = string.Empty;
                    return input;
                }
            return input;
        }
    }

    internal static string ValidateId(string input)
    {
        if (input.Length < 2 && input.Length > 4 && !string.IsNullOrWhiteSpace(input))
        {
            input = string.Empty;
            return input;
        }
        else
        {
            foreach (var letter in input)
                if (!char.IsLetterOrDigit(letter))
                {
                    input = string.Empty;
                    return input;
                }
        }
        return input;
    }
}
