﻿using Airlines.Business.Exceptions;

namespace Airlines.Business.DataClasses;
public class Flight(string identifier, string departureAirport, string arrivalAirport, IAircraft aircraftModel, double price, double timeinHours)
{
    public string Identifier => identifier;
    public string DepartureAirport => departureAirport;
    public string ArrivalAirport => arrivalAirport;
    public IAircraft AircraftModel => aircraftModel;
    public double Price => price;
    public double TimeinHours => timeinHours;

    internal static bool ValidateFlightCode(string flightCode)
    {
        if (string.IsNullOrWhiteSpace(flightCode) || flightCode == string.Empty)
        {
            throw new InvalidFlightCodeException();
        }
        foreach (var letter in flightCode)
            if (!char.IsLetterOrDigit(letter))
            {
                throw new InvalidFlightCodeException();
            }
        return true;
    }
}

