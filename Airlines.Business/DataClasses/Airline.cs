﻿using Airlines.Business.Exceptions;

namespace Airlines.Business.DataClasses;
public class Airline(string name)
{
    public string Name { get; set; } = name;
    internal static bool ValidateAirline(string? arName)
    {
        if (string.IsNullOrEmpty(arName) || arName.Length >= 6)
        {
            throw new InvalidAirlineNameException();
        }
        return true;
    }
}
