﻿using Airlines.Business.DtoModels;
using Airlines.Business.Mappers;
using Airlines.Business.Services.Interfaces;
using Airlines.Persistence.InMemory.EntitiesRepository.Interfaces;

namespace Airlines.Business.Services;
public class AirlineService : IAirlineService
{
    private readonly IAirlineRepository _airlineRepository;
    private readonly AirlineMapper _mapper;

    public AirlineService(IAirlineRepository airlineRepository, AirlineMapper mapper)
    {
        _airlineRepository = airlineRepository;
        _mapper = mapper;
    }

    public async Task<AirlineDTO?> GetAirlineByIdAsync(int id)
    {
        var airline = await _airlineRepository.GetAirlineByIdAsync(id);
        return airline != null ? _mapper.MapAirline(airline) : null;
    }

    public async Task<List<AirlineDTO>> GetAllAirlinesAsync()
    {
        var airlines = await _airlineRepository.GetAllAirlinesAsync();
        return airlines.Select(_mapper.MapAirline).ToList();
    }

    public async Task<List<AirlineDTO>> GetAllAirlinesAsync(string filter, string value)
    {
        var airlines = await _airlineRepository.GetAllAirlinesByFilterAsync(filter, value);
        return airlines.Select(_mapper.MapAirline).ToList();
    }

    public async Task<AirlineDTO?> AddAirlineAsync(AirlineDTO AirlineDTO)
    {
        var airline = await _airlineRepository.AddAirlineAsync(_mapper.MapAirline(AirlineDTO));
        return airline != null ? _mapper.MapAirline(airline) : null;
    }

    public async Task<AirlineDTO?> UpdateAirlineAsync(AirlineDTO updatedAirline)
    {
        var targetAirline = _airlineRepository.GetAirlineByIdAsync(updatedAirline.ID);

        if (targetAirline == null)
            return null;

        var airline = await _airlineRepository.UpdateAirlineAsync(_mapper.MapAirline(updatedAirline));
        return airline != null ? _mapper.MapAirline(airline) : null;
    }

    public async Task<AirlineDTO?> UpdateAirlineAsync(int id, AirlineDTO updatedAirline)
    {
        var airline = await _airlineRepository.UpdateAirlineAsync(id, _mapper.MapAirline(updatedAirline));
        return airline != null ? _mapper.MapAirline(airline) : null;
    }

    public async Task<AirlineDTO?> DeleteAirlineAsync(int id)
    {
        var airline = await _airlineRepository.DeleteAirlineAsync(id);
        return airline != null ? _mapper.MapAirline(airline) : null;
    }

    public async Task<int> GetAirlinesCountAsync() => await _airlineRepository.GetAirlinesCountAsync();

    public async Task<bool> IsAirlineNameUniqueAsync(string name) => name != null && await _airlineRepository.IsAirlineNameUniqueAsync(name);

    public bool IsAirlineNameLengthValid(string? name) => name != null && name.Length <= 6;
}