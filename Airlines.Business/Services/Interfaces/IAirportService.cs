﻿using Airlines.Business.DtoModels;

namespace Airlines.Business.Services.Interfaces;
public interface IAirportService
{
    Task<AirportDTO?> GetAirportByIdAsync(int id);
    Task<List<AirportDTO>> GetAllAirportsAsync();
    Task<List<AirportDTO>> GetAllAirportsAsync(string filter, string value);
    Task<AirportDTO?> AddAirportAsync(AirportDTO AirportDTO);
    Task<AirportDTO?> UpdateAirportAsync(AirportDTO updatedAirport);
    Task<AirportDTO?> UpdateAirportAsync(int id, AirportDTO updatedAirport);
    Task<AirportDTO?> DeleteAirportAsync(int id);
    Task<int> GetAirportsCountAsync();
    Task<bool> IsAirportCodeUniqueAsync(string code);
    Task<bool> IsAirportNameUniqueAsync(string name);
    bool IsAirportCodeLengthValid(string? code);
}