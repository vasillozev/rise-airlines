﻿using Airlines.Business.DtoModels;

namespace Airlines.Business.Services.Interfaces;

public interface IAirlineService
{
    Task<AirlineDTO?> GetAirlineByIdAsync(int id);
    Task<List<AirlineDTO>> GetAllAirlinesAsync();
    Task<List<AirlineDTO>> GetAllAirlinesAsync(string filter, string value);
    Task<AirlineDTO?> AddAirlineAsync(AirlineDTO AirlineDTO);
    Task<AirlineDTO?> UpdateAirlineAsync(AirlineDTO updatedAirline);
    Task<AirlineDTO?> UpdateAirlineAsync(int id, AirlineDTO updatedAirline);
    Task<AirlineDTO?> DeleteAirlineAsync(int id);
    Task<int> GetAirlinesCountAsync();
    Task<bool> IsAirlineNameUniqueAsync(string name);
    public bool IsAirlineNameLengthValid(string? name);

}