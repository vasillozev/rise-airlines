﻿using Airlines.Business.DtoModels;

namespace Airlines.Business.Services.Interfaces;
public interface IFlightService
{
    Task<FlightDTO?> GetFlightByIdAsync(int id);
    Task<List<FlightDTO>> GetAllFlightsAsync();
    Task<List<FlightDTO>> GetAllFlightsAsync(string filter, string value);
    Task<List<FlightDTO>> GetAllFlightForTimePeriod(string timePeriod);
    Task<FlightDTO?> AddFlightAsync(FlightDTO FlightDTO);
    Task<FlightDTO?> UpdateFlightAsync(FlightDTO updatedFlight);
    Task<FlightDTO?> UpdateFlightAsync(int id, FlightDTO updatedFlight);
    Task<FlightDTO?> DeleteFlightAsync(int id);
    Task<int> GetFlightsCountAsync();
    bool IsArrivalDateInTheFuture(DateTime arrivalDateTime);
    bool IsDepartureDateInTheFuture(DateTime departureDateTime);
    bool IsArrivalDateAfterDeprtureDate(DateTime departureDateTime, DateTime arrivalDateTime);
}