﻿using Airlines.Business.DtoModels;
using Airlines.Business.Mappers;
using Airlines.Business.Services.Interfaces;
using Airlines.Persistence.InMemory.Entities;
using Airlines.Persistence.InMemory.EntitiesRepository.Interfaces;

namespace Airlines.Business.Services;
public class FlightService : IFlightService
{
    private readonly IFlightRepository _flightRepository;
    private readonly FlightMapper _mapper;

    public FlightService(IFlightRepository flightRepository, FlightMapper flightMapper)
    {
        _flightRepository = flightRepository;
        _mapper = flightMapper;
    }

    public async Task<FlightDTO?> GetFlightByIdAsync(int id)
    {
        var flight = await _flightRepository.GetFlightByIdAsync(id);
        return flight != null ? _mapper.MapFlight(flight) : null;
    }

    public async Task<List<FlightDTO>> GetAllFlightsAsync()
    {
        var flights = await _flightRepository.GetAllFlightsAsync();
        return flights.Select(_mapper.MapFlight).ToList();
    }

    public async Task<List<FlightDTO>> GetAllFlightsAsync(string filter, string value)
    {


        var flights = await _flightRepository.GetAllFlightsByFilterAsync(filter, value);
        return flights.Select(_mapper.MapFlight).ToList();
    }

    public async Task<List<FlightDTO>> GetAllFlightForTimePeriod(string timePeriod)
    {
        List<Flight> flights = [];

        if (timePeriod == "day")
            flights = await _flightRepository.GetAllFlightsForTodayAsync();

        else if (timePeriod == "week")
            flights = await _flightRepository.GetAllFlightsForThisWeekAsync();

        else if (timePeriod == "month")
            flights = await _flightRepository.GetAllFlightsForThisMonthAsync();

        return flights.Select(_mapper.MapFlight).ToList(); 
    }

    public async Task<FlightDTO?> AddFlightAsync(FlightDTO FlightDTO)
    {
        var flight = await _flightRepository.AddFlightAsync(_mapper.MapFlight(FlightDTO));
        return flight != null ? _mapper.MapFlight(flight) : null;
    }

    public async Task<FlightDTO?> UpdateFlightAsync(FlightDTO updatedFlight)
    {
        var targetFlight = _flightRepository.GetFlightByIdAsync(updatedFlight.FlightId);

        if (targetFlight == null)
            return null;

        var flight = await _flightRepository.UpdateFlightAsync(_mapper.MapFlight(updatedFlight));
        return flight != null ? _mapper.MapFlight(flight) : null;
    }

    public async Task<FlightDTO?> UpdateFlightAsync(int id, FlightDTO updatedFlight)
    {
        var flight = await _flightRepository.UpdateFlightAsync(id, _mapper.MapFlight(updatedFlight));
        return flight != null ? _mapper.MapFlight(flight) : null;
    }

    public async Task<FlightDTO?> DeleteFlightAsync(int id)
    {
        var flight = await _flightRepository.DeleteFlightAsync(id);
        return flight != null ? _mapper.MapFlight(flight) : null;
    }

    public async Task<int> GetFlightsCountAsync() => await _flightRepository.GetFlightsCountAsync();

    public bool IsArrivalDateAfterDeprtureDate(DateTime departureDateTime, DateTime arrivalDateTime) => departureDateTime >= arrivalDateTime;

    public bool IsArrivalDateInTheFuture(DateTime arrivalDateTime) => arrivalDateTime > DateTime.UtcNow;

    public bool IsDepartureDateInTheFuture(DateTime departureDateTime) => departureDateTime > DateTime.UtcNow;
}