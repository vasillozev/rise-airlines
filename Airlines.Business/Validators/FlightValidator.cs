﻿using Airlines.Business.DtoModels;
using Airlines.Persistence.InMemory.Exceptions;

namespace Airlines.Business.Validators;
public static class FlightValidator
{
    public static bool ValidateFlight(FlightDTO flight)
    {
        try
        {
            return flight != null
                   && FlightValidation(flight.FlightNumber);

        }
        catch (InvalidAirportException ex)
        {
            Console.WriteLine($"{ex.Message}");
            return false;
        }
    }

    public static bool FlightValidation(string check)
        => !string.IsNullOrEmpty(check) && check.All(char.IsLetterOrDigit) && check.Length == 5;

    public static bool AircraftValidation(string check)
        => !string.IsNullOrEmpty(check);

    public static bool ExistingAirportValidation(string check)
        => !string.IsNullOrEmpty(check) && check.All(char.IsLetter) && check.Length >= 2 && check.Length <= 4;
}
