﻿using Airlines.Business.DtoModels;

namespace Airlines.Business.Validators;
public class AirlinesValidator
{
    public static bool ValidateAirline(AirlineDTO airline)
    {
        try
        {
            return !AirlineValidation(airline) ? throw new Persistence.InMemory.Exceptions.InvalidAirlineNameException("Airline validation failed.") : true;
        }
        catch (Persistence.InMemory.Exceptions.InvalidAirlineNameException ex)
        {
            Console.WriteLine($"{ex.Message}");
            return false;
        }
    }

    public static bool AirlineValidation(AirlineDTO airlineDTO)
    => airlineDTO.Name.Length < 6 && !string.IsNullOrEmpty(airlineDTO.Description) && airlineDTO.Fleet_size > 0 && airlineDTO.Founded is DateOnly;
}
