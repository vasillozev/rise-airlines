﻿using AutoMapper;
using Airlines.Business.DtoModels;
using Airlines.Persistence.InMemory.Entities;

namespace Airlines.Business.Profiles;
public class FlightProfile : Profile
{
    public FlightProfile()
    {
        CreateMap<Flight, FlightDTO>().ReverseMap();
        CreateMap<Airport, AirportDTO>().ReverseMap();
        CreateMap<FlightDTO, Flight>().ReverseMap();
        CreateMap<AirportDTO, Airport>().ReverseMap();
    }
}