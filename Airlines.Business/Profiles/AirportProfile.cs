﻿using AutoMapper;
using Airlines.Business.DtoModels;
using Airlines.Persistence.InMemory.Entities;

namespace Airlines.Business.Profiles;
public class AirportProfile : Profile
{
    public AirportProfile()
    {
        CreateMap<Airport, AirportDTO>().ReverseMap();
        CreateMap<AirportDTO, Airport>().ReverseMap();
    }
}