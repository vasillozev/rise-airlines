﻿using System.ComponentModel.DataAnnotations;

namespace Airlines.Business.DtoModels;
public class FlightDTO
{

    public int FlightId { get; set; }

    [Required(ErrorMessage = "Please provide the flight number.")]
    [StringLength(5, ErrorMessage = "Number must be up to 5 characters.")]
    public string? FlightNumber { get; set; }

    [Required(ErrorMessage = "Please select the departure airport.")]
    public int FromAirportID { get; set; }

    [Required(ErrorMessage = "Please select the arrival airport.")]
    public int ToAirportID { get; set; }

    [Required(ErrorMessage = "Please provide the departure date and time.")]
    public DateTime DepartureDateTime { get; set; }

    [Required(ErrorMessage = "Please provide the arrival date and time.")]
    public DateTime ArrivalDateTime { get; set; }

    public virtual AirportDTO? ToAirport { get; set; }

    public virtual AirportDTO? FromAirport { get; set; }
}
