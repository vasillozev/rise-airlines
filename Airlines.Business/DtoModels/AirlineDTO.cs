﻿using System.ComponentModel.DataAnnotations;

namespace Airlines.Business.DtoModels;
public class AirlineDTO
{
#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
    public AirlineDTO()
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
    {
    }
    public AirlineDTO(string name, DateOnly? founded, int? fleetSize, string description)
    {
        Name = name;
        Founded = founded;
        Fleet_size = fleetSize;
        Description = description;
    }

    public int ID { get; set; }

    public string Name { get; set; }
    public DateOnly? Founded { get; set; }
#pragma warning disable CA1707 // Identifiers should not contain underscores
    public int? Fleet_size { get; set; }

#pragma warning restore CA1707 // Identifiers should not contain underscores
    public string Description { get; set; }
}
