﻿using Airlines.Business.AlgorithmClasses;
using Airlines.Business.DataClasses;
using Airlines.Business.Exceptions;

namespace Airlines.Business.ReservationBookingSystem;
public class ReservationBookingSystemCommander
{
    internal static Dictionary<string, Airport> AirportListToDictionary(IList<string[]> airportsValues)
    {
        Dictionary<string, Airport> cityCountryAirports = [];
        foreach (var airport in airportsValues)
        {
            if (Airport.ValidateId(airport[0]) == "" || Airport.ValidateInput(airport[1]) == ""
                || Airport.ValidateInput(airport[2]) == "" || cityCountryAirports.ContainsKey(airport[1]))
            {
                continue;
            }
            cityCountryAirports.Add(airport[1], new Airport(airport[0], airport[1], airport[2], airport[3]));
        }
        return cityCountryAirports;
    }

    internal static HashSet<string> AirlineListToHashSet(IList<string[]> airlinesValues)
    {
        HashSet<string> airlinesSetNames = [];
        foreach (var item in airlinesValues)
        {
            if (airlinesSetNames.Contains(item[0]))
            {
                continue;
            }
            _ = airlinesSetNames.Add(item[0]);
        }
        return airlinesSetNames;
    }
    internal static HashSet<Flight> FlightListToHashSet(IList<string[]> flightsValues, HashSet<IAircraft> setAircrafts)
    {
        if (flightsValues == null)
        {
            throw new InvalidFileExceptionSearchRoute();
        }
        HashSet<Flight> flightsSet = [];
        foreach (var item in flightsValues)
        {
            IAircraft? aircraft = setAircrafts.First(x => x.AircraftModel == item[3]);
            _ = flightsSet.Add(new Flight(item[0], item[1], item[2], aircraft, Convert.ToDouble(item[4]), Convert.ToDouble(item[5])));
        }
        return flightsSet;
    }
    internal static HashSet<IAircraft> AircraftListToHashSet(IList<string[]> aircraftsValues)
    {
        HashSet<IAircraft> aircraftsSet = [];
        int count = 0;
        foreach (var aircraft in aircraftsValues)
        {
            foreach (var value in aircraft)
            {
                if (value == "-")
                {
                    count++;
                }
            }
            _ = count == 0
                ? aircraftsSet.Add(new PassengerAircraft(aircraft[0], double.Parse(aircraft[1]), double.Parse(aircraft[2]), int.Parse(aircraft[3])))
                : count == 1
                    ? aircraftsSet.Add(new CargoAircraft(aircraft[0], double.Parse(aircraft[1]), double.Parse(aircraft[2])))
                    : aircraftsSet.Add(new PrivateAircraft(aircraft[0], int.Parse(aircraft[3])));
        }
        return aircraftsSet;
    }

    internal static void ReserveCargo(string inputCommand, HashSet<Flight> setFlights)
    {
        string[] commandValues = inputCommand.Split(", ");
        string flightIdentifier = "";
        flightIdentifier = commandValues[2];
        double cargoWeight = 0;
        double cargoVolume = 0;
        if (commandValues.Length != 5 || !ConsoleValidator.Validate(commandValues[3])
            || !ConsoleValidator.Validate(commandValues[4]))
        {
            throw new InvalidCargoFormatException();
        }
        cargoWeight = double.Parse(commandValues[3]);
        cargoVolume = double.Parse(commandValues[4]);
        Flight? flight = setFlights.FirstOrDefault(x => x.Identifier == flightIdentifier);
        if (flight != null)
        {
            if (flight.AircraftModel.CargoWeight >= cargoWeight
                && flight.AircraftModel.CargoVolume >= cargoVolume)
            {
                flight.AircraftModel.CargoWeight -= cargoWeight;
                flight.AircraftModel.CargoVolume -= cargoVolume;
            }
        }
    }
    internal static void ReserveTicket(string inputCommand, HashSet<Flight> setFlights)
    {
        string[] commands = inputCommand.Split(", ");
        if (commands.Length != 6 || !Flight.ValidateFlightCode(commands[2]) ||
            !ConsoleValidator.Validate(commands[3]) || !ConsoleValidator.Validate(commands[4])
            || !ConsoleValidator.Validate(commands[5]) || !setFlights.Any(x => x.Identifier == commands[2]))
        {
            throw new InvalidReserveTicketFormatException();
        }
        string flightIdentifier = commands[2];
        int seats = int.Parse(commands[3]);
        int smallBaggageCount = int.Parse(commands[4]);
        int largeBaggageCount = int.Parse(commands[5]);
        var flight = setFlights.First(x => x.Identifier == commands[2]);
        if (flight.AircraftModel.Seats >= int.Parse(commands[3]))
        {
            flight.AircraftModel.Seats -= int.Parse(commands[3]);
        }
    }
    internal static FlightNode FlightRoutesFactory(IList<string[]> strings, HashSet<Flight> flights)
    {
        FlightNode? flightNode = null;
        foreach (var stringLine in strings)
        {
            if (flightNode == null)
            {
                flightNode = new FlightNode(flights.First(x => x.DepartureAirport == stringLine[0]));
                continue;
            }

            if (!flights.Any(x => x.Identifier == stringLine[0]))
            {
                throw new InvalidFileExceptionSearchRoute();
            }

            FlightNode flightNodeChild = new FlightNode(flights.First(x => x.Identifier == stringLine[0]));
            _ = flightNode.AddChild(flightNodeChild.Value);
        }
        if (flightNode == null)
        {
            throw new InvalidFileExceptionSearchRoute();
        }
        return flightNode;
    }
    internal static Dictionary<string, List<FlightNode>> FlightRoutesAirportsFactory(Dictionary<string, List<FlightNode>> airportFlights, HashSet<Flight> flights)
    {
        foreach (var airport in airportFlights)
        {
            foreach (var item in flights.Where(x => x.DepartureAirport == airport.Key))
            {
                airportFlights[airport.Key].Add(new FlightNode(item));
            }
            foreach (var flightNode in airportFlights[airport.Key])
            {
                foreach (var item in airport.Value)
                {
                    if (flightNode.Value.ArrivalAirport == item.Value.DepartureAirport)
                    {
                        _ = flightNode.AddChild(item.Value);
                    }
                }
            }
        }
        return airportFlights;
    }
    internal static Queue<FlightNode> FlightRouteFind(string input, FlightNode flights)
    {
        string[] inputVlaues = input.Split(" ");
        if (inputVlaues.Length != 3)
        {
            throw new RouteFindException();
        }
        string destination = inputVlaues[2];
        Queue<FlightNode> flightNodes = new Queue<FlightNode>();
        foreach (var item in flights.Traverse(destination))
        {
            if (item != null)
            {
                flightNodes.Enqueue(item);
            }
        }
        return flightNodes;
    }
}