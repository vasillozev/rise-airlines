﻿using AutoMapper;
using Airlines.Business.DtoModels;
using Airlines.Persistence.InMemory.Entities;

namespace Airlines.Business.Mappers;
public class FlightMapper
{
    private readonly IMapper _mapper;

    public FlightMapper(IMapper mapper) => _mapper = mapper;
    public Flight MapFlight(FlightDTO flightDto)
    {
        var flight = _mapper.Map<Flight>(flightDto);
        return flight;
    }

    public FlightDTO MapFlight(Flight flight)
    {
        var flightDto = _mapper.Map<FlightDTO>(flight);

        return flightDto;
    }
}