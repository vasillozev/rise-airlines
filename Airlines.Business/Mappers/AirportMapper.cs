﻿using AutoMapper;
using Airlines.Business.DtoModels;
using Airlines.Persistence.InMemory.Entities;

namespace Airlines.Business.Mappers;
public class AirportMapper
{
    private readonly IMapper _mapper;

    public AirportMapper(IMapper mapper) => _mapper = mapper;
    public Airport MapAirport(AirportDTO airportDto)
    {
        var airport = _mapper.Map<Airport>(airportDto);
        return airport;
    }

    public AirportDTO MapAirport(Airport airport)
    {
        var airportDto = _mapper.Map<AirportDTO>(airport);

        return airportDto;
    }
}