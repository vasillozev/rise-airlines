﻿using AutoMapper;
using Airlines.Business.DtoModels;
using Airlines.Persistence.InMemory.Entities;

namespace Airlines.Business.Mappers;
public class AirlineMapper
{
    private readonly IMapper _mapper;

    public AirlineMapper(IMapper mapper) => _mapper = mapper;
    public Airline MapAirline(AirlineDTO airlineDto)
    {
        var airline = _mapper.Map<Airline>(airlineDto);
        return airline;
    }

    public AirlineDTO MapAirline(Airline airline)
    {
        var airlineDto = _mapper.Map<AirlineDTO>(airline);

        return airlineDto;
    }
}