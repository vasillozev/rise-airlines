﻿namespace Airlines.Business.AlgorithmClasses;
internal class TimeCalculator
{
    internal Dictionary<Node, double> Times;

    internal Dictionary<Node, Node> Routes;
    internal GraphFLights graph;
    internal List<Node> AllNodes;

    internal TimeCalculator(GraphFLights g)
    {
        this.graph = g;
        this.AllNodes = g.GetNodes();
        Times = SetTimes();
        Routes = SetRoutes();
    }
    private Dictionary<Node, double> SetTimes()
    {
        Dictionary<Node, double> Times = [];

        foreach (Node n in graph.GetNodes())
        {
            Times.Add(n, double.MaxValue);
        }
        return Times;
    }

    private Dictionary<Node, Node> SetRoutes()
    {
        Dictionary<Node, Node> routes = [];

        foreach (Node n in graph.GetNodes())
        {
            routes.Add(n, null);
        }
        return routes;
    }
    public void Calculate(Node source, Node destination)
    {
        Times[source] = 0;

        while (AllNodes.ToList().Count != 0)
        {
            Node leastExpensiveNode = GetLeastExpensiveNode();
            ExamineConnections(leastExpensiveNode);
            _ = AllNodes.Remove(leastExpensiveNode);
        }
        Print(source, destination);
    }
    internal void ExamineConnections(Node n)
    {
        foreach (var neighbor in n.getNeighbors())
        {
            if (Times[n] + neighbor.Value < Times[neighbor.Key])
            {
                Times[neighbor.Key] = neighbor.Value + Times[n];
                Routes[neighbor.Key] = n;
            }
        }
    }
    internal Node GetLeastExpensiveNode()
    {
        Node leastExpensive = AllNodes.FirstOrDefault();

        foreach (var n in AllNodes)
        {
            if (Times[n] < Times[leastExpensive])
                leastExpensive = n;
        }
        return leastExpensive;
    }
    private void Print(Node source, Node destination)
    {
        Console.WriteLine(string.Format("The least possible time for flying from {0} to {1} is: {2}", source.Flight.DepartureAirport, destination.Flight.DepartureAirport, Times[destination]));
        PrintLeg(destination);
    }

    private void PrintLeg(Node d)
    {
        if (Routes[d] == null)
            return;
        Console.WriteLine(string.Format("{0} <-- {1}", d.getName(),
        Routes[d].getName()));
        PrintLeg(Routes[d]);
    }
}
