﻿namespace Airlines.Business.AlgorithmClasses;
public static class Reader
{
    internal static IList<string[]> ReadInput(string path)
    {
        List<string[]> values = [];
        using (var reader = new StreamReader(path))
        {
            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                if (line != null)
                {
                    values.Add(line.Split(','));
                }
            }
        }
        return values;
    }
}
