﻿using Airlines.Business.DataClasses;
internal class Node
{
    public Flight Flight;
    private Dictionary<Node, double> Neighbors;

    public Node(Flight flight)
    {
        Flight = flight;
        Neighbors = [];
    }

    public void AddNeighbour(Node n, double cost)
    {
        Neighbors.Add(n, cost);
    }
    public string getName()
    {
        return Flight.Identifier;
    }
    public Dictionary<Node, double> getNeighbors()
    {
        return Neighbors;
    }
}