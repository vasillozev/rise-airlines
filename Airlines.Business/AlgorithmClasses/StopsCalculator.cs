﻿namespace Airlines.Business.AlgorithmClasses;
internal class StopsCalculator
{
    internal Dictionary<Node, double> Prices;

    internal Dictionary<Node, Node> Routes;
    internal GraphFLights graph;
    internal List<Node> AllNodes;

    internal StopsCalculator(GraphFLights g)
    {
        this.graph = g;
        this.AllNodes = g.GetNodes();
        Prices = SetPrices();
        Routes = SetRoutes();
    }
    private Dictionary<Node, double> SetPrices()
    {
        Dictionary<Node, double> Prices = [];

        foreach (Node n in graph.GetNodes())
        {
            Prices.Add(n, double.MaxValue);
        }
        return Prices;
    }

    private Dictionary<Node, Node> SetRoutes()
    {
        Dictionary<Node, Node> routes = [];

        foreach (Node n in graph.GetNodes())
        {
            routes.Add(n, null);
        }
        return routes;
    }
    public void Calculate(Node source, Node destination)
    {
        Prices[source] = 0;

        while (AllNodes.ToList().Count != 0)
        {
            Node leastExpensiveNode = GetLeastExpensiveNode();
            ExamineConnections(leastExpensiveNode);
            _ = AllNodes.Remove(leastExpensiveNode);
        }
        Print(source, destination);
    }
    private void ExamineConnections(Node n)
    {
        foreach (var neighbor in n.getNeighbors())
        {
            if (Prices[n] + neighbor.Value < Prices[neighbor.Key])
            {
                Prices[neighbor.Key] = neighbor.Value + Prices[n];
                Routes[neighbor.Key] = n;
            }
        }
    }

    private Node GetLeastExpensiveNode()
    {
        Node leastExpensive = AllNodes.FirstOrDefault();

        foreach (var n in AllNodes)
        {
            if (Prices[n] < Prices[leastExpensive])
                leastExpensive = n;
        }
        return leastExpensive;
    }
    private void Print(Node source, Node destination)
    {
        Console.WriteLine(string.Format("The least possible cost for flying from {0} to {1} is:", source.Flight.DepartureAirport, destination.Flight.DepartureAirport));
        PrintLeg(destination);
    }

    private void PrintLeg(Node d)
    {
        if (Routes[d] == null)
            return;
        Console.WriteLine(string.Format("{0} <-- {1}", d.getName(),
        Routes[d].getName()));
        PrintLeg(Routes[d]);
    }
}
