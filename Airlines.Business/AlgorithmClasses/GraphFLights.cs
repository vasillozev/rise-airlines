﻿namespace Airlines.Business.AlgorithmClasses;
internal class GraphFLights
{
    internal List<Node> Nodes;

    internal GraphFLights()
    {
        Nodes = new List<Node>();
    }

    internal void Add(Node n)
    {
        Nodes.Add(n);
    }

    internal void Remove(Node n)
    {
        Nodes.Remove(n);
    }

    internal List<Node> GetNodes()
    {
        return Nodes.ToList();
    }

    internal int getCount()
    {
        return Nodes.Count;
    }
}
