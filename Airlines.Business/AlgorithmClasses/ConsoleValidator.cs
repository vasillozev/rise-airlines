﻿using Airlines.Business.Exceptions;

namespace Airlines.Business.AlgorithmClasses;
public static class ConsoleValidator
{
    internal static bool Validate(string? input)
    {
        if (string.IsNullOrEmpty(input) || input == " ")
        {
            throw new InvalidInputException();
        }
        return true;
    }
}
