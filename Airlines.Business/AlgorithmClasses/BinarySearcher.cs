﻿namespace Airlines.Business.AlgorithmClasses;
public static class BinarySearcher
{
    //Function for binary search
    internal static bool BinarySearch<T>(SortedList<string, T> sortedList, string key)
    {
        var min = 0;
        var max = sortedList.Count - 1;
        while (min <= max)
        {
            var mid = (min + max) / 2;
            if (key == sortedList.GetKeyAtIndex(mid))
                return true;
            else if (key.CompareTo(sortedList.GetKeyAtIndex(mid)) == -1)
                max = mid - 1;
            else
                min = mid + 1;
        }
        return false;
    }
}
