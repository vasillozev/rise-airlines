﻿using Airlines.Business.DataClasses;
using Airlines.Business.ReservationBookingSystem;
#pragma warning disable CA1854 // Prefer the 'IDictionary.TryGetValue(TKey, out TValue)' method

namespace Airlines.Business.AlgorithmClasses
{
    internal class Graph
    {
        public Dictionary<string, List<FlightNode>> AirportsFlights { get; set; } = [];
        public HashSet<Flight> Flights { get; set; } = [];
        private Flight Value { get; }
        internal Queue<FlightNode> Children { get; } = new();
        public Graph(HashSet<Flight> flights)
        {
            foreach (var flight in flights)
            {
                FlightNode flightNode = new FlightNode(flight);
                if (!AirportsFlights.ContainsKey(flight.DepartureAirport))
                {
                    AirportsFlights.Add(flight.DepartureAirport, []);
                }
                AirportsFlights[flight.DepartureAirport].Add(flightNode);
                foreach (var flightNodeEntity in AirportsFlights[flight.DepartureAirport])
                {
                    if (flightNodeEntity.AddChild(flight))
                    {
                        flightNodeEntity.Children.Enqueue(new FlightNode(flight));
                    }
                }
            }
        }
        internal HashSet<FlightNode> Traverse(string startAirport, string endAirport, Dictionary<string, List<FlightNode>> airportsFlights)
        {
            HashSet<FlightNode> strings = [];
            Queue<FlightNode> ndoes = [];
            foreach (var flightNode in airportsFlights[startAirport])
            {
                ndoes = ReservationBookingSystemCommander.FlightRouteFind($"please find {endAirport}", flightNode);
                while (ndoes.Count > 0)
                {
                    strings.Add(ndoes.Dequeue());
                }
            }
            return strings;
        }
        internal bool RouteCheck(string startAirpoint, string endAirport)
        {
            Queue<FlightNode> flights = [];
            foreach (var item in AirportsFlights[startAirpoint])
            {
                flights = ReservationBookingSystemCommander.FlightRouteFind(endAirport, item);
                if (flights.Count > 0)
                {
                    return true;
                }
            }
            return false;
        }
        internal Queue<FlightNode> RouteSearch(string startAirpoint, string endAirport)
        {
            Queue<FlightNode> queue = [];
            foreach (var flightNode in AirportsFlights[startAirpoint])
            {
                Queue<FlightNode> flights = flightNode.Traverse(endAirport);
                while (flights.Count > 0)
                {
                    queue.Enqueue(flights.Dequeue());
                }
            }
            return queue;
        }
    }
}