﻿using System.Collections;

namespace Airlines.Business.AlgorithmClasses;
public class SingleLinkedList<T> : IEnumerable<T>
{
    private class Node(T flight)
    {
        public T Flight { get; } = flight;
        public Node? Next { get; set; }
    }
    private Node? _head;
    private Node? _tail;

    internal void Add(T identifier)
    {
        var newNode = new Node(identifier);
        if (_head == null)
        {
            _head = newNode;
            _tail = newNode;
        }
        else
        {
            if (_tail == null)
            {
                return;
            }
            _tail.Next = newNode;
            _tail = newNode;
        }
    }

    internal void RemoveEnd()
    {
        if (_head == null)
            return;
        else if (_head == _tail)
        {
            _head = null;
            _tail = null;
        }
        else
        {
            var current = _head;

            while (current != null && current.Next != _tail)
                current = current.Next;
            if (current == null)
            {
                return;
            }
            current.Next = null;
            _tail = current;
        }
    }
    public IEnumerator<T> GetEnumerator()
    {
        var current = _head;
        while (current != null)
        {
            yield return current.Flight;
            current = current.Next;
        }
    }
    IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
}
