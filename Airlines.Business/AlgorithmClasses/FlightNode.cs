﻿using Airlines.Business.DataClasses;

namespace Airlines.Business.AlgorithmClasses;

internal class FlightNode
{
    private Queue<FlightNode> _children { get; set; } = new Queue<FlightNode>();
    public FlightNode(Flight value) => Value = value;
    Queue<FlightNode> childrenArray = [];
    internal Flight Value { get; }
    internal Queue<FlightNode> Children => _children;
    internal bool AddChild(Flight value)
    {
        var node = new FlightNode(value);
        if (Value.ArrivalAirport == node.Value.DepartureAirport)
        {
            _children.Enqueue(node);
        }
        foreach (var child in _children)
        {
            if (child.AddChild(value))
            {
                child.Children.Enqueue(node);
            }
        }
        return false;
    }
    internal Queue<FlightNode> Traverse(string destination)
    {
        if (Value.ArrivalAirport == destination)
        {
            childrenArray.Enqueue(this);
            return childrenArray;
        }
        foreach (var node in _children)
        {
            childrenArray.Enqueue(this);
            childrenArray.Enqueue(node);
           Queue<FlightNode> queue1 = node.Traverse(destination);
            while (queue1.Count > 0)
            {
                childrenArray.Enqueue(queue1.Dequeue());
            }
        }
        return childrenArray;
    }
    public IEnumerable<string> Flatten() => new[] { Value.Identifier }.Concat(Children.SelectMany(x => x.Flatten()));
}
