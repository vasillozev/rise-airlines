﻿namespace Airlines.Business.Exceptions;
public class InvalidAirlineNameException : Exception
{
    public new string Message { get; set; } = "Invalid airline name. Format is: exist <airline name>";
}
