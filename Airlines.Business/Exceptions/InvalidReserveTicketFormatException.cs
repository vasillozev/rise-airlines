﻿namespace Airlines.Business.Exceptions;
internal class InvalidReserveTicketFormatException : Exception
{
    public new string Message { get; set; } =

    "Please type the command in the format :" +
                "reserve ticket <Flight Identifier> <Seats> <Small Baggage Count> <Large Baggage Count>";
}
