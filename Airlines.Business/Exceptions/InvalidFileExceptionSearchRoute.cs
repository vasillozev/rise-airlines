﻿
namespace Airlines.Business.Exceptions;
internal class InvalidFileExceptionSearchRoute : FileNotFoundException
{
    public InvalidFileExceptionSearchRoute()
    {
    }

    public InvalidFileExceptionSearchRoute(string? message) : base(message)
    {
    }

    public InvalidFileExceptionSearchRoute(string? message, string fileName) : base(message)
    {
    }

    public new string Message { get; set; } = "Invalid file path.";
}
