﻿namespace Airlines.Business.Exceptions;
internal class InvalidFlightsCommandFormatException : Exception
{
    public InvalidFlightsCommandFormatException()
    {
    }

    public InvalidFlightsCommandFormatException(string? message) : base(message)
    {
    }

    public new string Message { get; set; } =

   "Please type the command in the format :" +
            "route new, or route add <Flight Identifier>, or route remove, or route print";
}
