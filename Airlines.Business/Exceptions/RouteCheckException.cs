﻿namespace Airlines.Business.Exceptions;
internal class RouteCheckException : Exception
{
    public new string Message { get; set; } = "Invalid route check exception. Format is: route check <Start Airport> <End Airport>";
}
