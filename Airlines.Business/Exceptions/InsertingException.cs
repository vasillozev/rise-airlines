﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airlines.Business.Exceptions;
internal class InsertingException : Exception
{
    public new string Message { get; set; } = "It is not successfull!";
}
