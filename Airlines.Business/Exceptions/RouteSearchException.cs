﻿namespace Airlines.Business.Exceptions;
internal class RouteSearchException : Exception
{
    public new string Message { get; set; } = "Invalid route search exception. Format is: route search <Start Airport> <End Airport>";
}
