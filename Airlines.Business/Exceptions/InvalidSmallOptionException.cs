﻿namespace Airlines.Business.Exceptions;
internal class InvalidSmallOptionException : Exception
{
    public new string Message { get; set; } =

        "Invalid small luggage format.";
}
