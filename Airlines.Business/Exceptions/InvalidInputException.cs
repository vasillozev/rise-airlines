﻿namespace Airlines.Business.Exceptions;
public class InvalidInputException : Exception
{
    public InvalidInputException()
    {

    }
    public InvalidInputException(string message) => Message = message;
    public new string Message { get; set; } = "Invalid input. Input cannot be null, empty or whitespace.";
}

