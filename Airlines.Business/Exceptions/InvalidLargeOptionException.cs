﻿namespace Airlines.Business.Exceptions;
internal class InvalidLargeOptionException : Exception
{
    public InvalidLargeOptionException()
    {
    }

    public InvalidLargeOptionException(string? message) : base(message)
    {
    }

    public new string Message { get; set; } =

        "Invalid large luggage format.";
}
