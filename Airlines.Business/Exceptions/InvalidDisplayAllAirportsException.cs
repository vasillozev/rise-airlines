﻿namespace Airlines.Business.Exceptions;
internal class InvalidDisplayAllAirportsException : Exception
{
    public InvalidDisplayAllAirportsException()
    {
    }

    public InvalidDisplayAllAirportsException(string? message) : base(message)
    {
    }

    public new string Message { get; set; } =

    "Please type the command in the format :" +
                        "list <input data> <from>";
}
