﻿namespace Airlines.Business.Exceptions;
internal class InvalidCargoFormatException : Exception
{
    public InvalidCargoFormatException()
    {
    }

    public InvalidCargoFormatException(string? message) : base(message)
    {
    }

    public new string Message { get; set; } =

    "Please type the command in the format :" +
                        "reserve cargo <Flight Identifier> <Cargo Weight> <Cargo Volume>";
}
