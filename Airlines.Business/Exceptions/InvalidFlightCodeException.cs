﻿namespace Airlines.Business.Exceptions;
public class InvalidFlightCodeException : Exception
{
    public new string Message { get; set; } = "Invalid flight code. Flight code must be string of alphanumeric characters.";
}
