﻿namespace Airlines.Business.Exceptions;
internal class RouteFindException : Exception
{
    public new string Message { get; set; } = "Invalid route find exception. Format is: route find <Destination Airport>";
}
