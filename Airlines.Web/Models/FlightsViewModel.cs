﻿using Airlines.Business.DtoModels;

namespace Airlines.Web.Models;

public class FlightsViewModel
{
    public required IEnumerable<FlightDTO> Flights { get; set; }
    public required IEnumerable<AirportDTO> Airports { get; set; }
}

