﻿using Airlines.Business.DtoModels;

namespace Airlines.Web.Models;

public class AirportViewModel
{
    public required IEnumerable<AirportDTO> Airports { get; set; }
}