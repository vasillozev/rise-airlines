﻿using Airlines.Business.DtoModels;

namespace Airlines.Web.Models;

public class AirlinesViewModel
{
    public required IEnumerable<AirlineDTO> Airlines { get; set; }
}