﻿const formElement = document.querySelector(".form-wrapper form");
const nameInputElement = document.querySelector("input[name='Name']");
const foundedInputElement = document.querySelector("input[name='Founded']");
const fleetSizeInputElement = document.querySelector("input[name='Fleet_size']");
const descriptionTextAreaElement = document.querySelector("textarea[name='Description']");
const submitButtonElement = document.querySelector(".submit-form-button");
const countryInputElement = document.querySelector("input[name='Country']");
const cityInputElement = document.querySelector("input[name='City']");
const codeInputElement = document.querySelector("input[name='Code']");
const runwaysInputElement = document.querySelector("input[name='Runways']");

const isValidForm = (currentValue) => currentValue === true;

const array1 = [validateName, validateFounded, validateFleetSize, validateDescription];

formElement.addEventListener("change", validateForm);
nameInputElement.addEventListener("blur", validateName);
foundedInputElement.addEventListener("blur", validateFounded);
fleetSizeInputElement.addEventListener("blur", validateFleetSize);
descriptionTextAreaElement.addEventListener("blur", validateDescription);

if (validateForm) {
    submitButtonElement.removeAttribute("disabled");
} else {
    submitButtonElement.setAttribute("disabled", true);
}

function validateForm() { 
    return array1.every(isValidForm);
}

function validateName() {
    let nameInput = document.getElementById('Name');
    let nameError = document.getElementById('name-error');
    let name = nameInput.value.trim();

    if (name === '' || name.length > 6) {
        nameError.textContent = 'Name is required and must be no more than 6 characters long';
        return false;
    }
    else {
        nameError.textContent = '';
        return true;
    }
}

function validateFounded() {
    let foundedInput = document.getElementById('Founded');
    let foundedError = document.getElementById('founded-error');
    let foundedDate = new Date(foundedInput.value.trim());
    let currentDate = new Date();

    if (isNaN(foundedDate) || foundedDate > currentDate) {
        foundedError.textContent = 'Please enter a valid date that cannot be in the future';
        return false;
    } else {
        foundedError.textContent = '';
        return true;
    }
}

function validateFleetSize() {
    let fleetSizeInput = document.getElementById('fleetSize');
    let fleetSizeError = document.getElementById('fleetSize-error');
    let fleetSize = parseInt(fleetSizeInput.value.trim(), 10);

    if (isNaN(fleetSize) || fleetSize < 0) {
        fleetSizeError.textContent = 'Fleet size is required and must be a positive number';
        return false;
    } else {
        fleetSizeError.textContent = '';
        return true;
    }
}

function validateDescription() {
    let descriptionInput = document.getElementById('Description');
    let descriptionError = document.getElementById('description-error');
    let description = descriptionInput.value.trim();

    if (description === '' || !/^[A-Za-z\s]+$/.test(description)) {
        descriptionError.textContent = 'Description is required and must contain letters and spaces only';
        return false;
    } else {
        descriptionError.textContent = '';
        return true;
    }
}